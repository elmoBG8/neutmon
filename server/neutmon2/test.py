from abc import ABCMeta, abstractmethod
import http_server
import logging
import time
import random
import struct
import os

logger = logging.getLogger(__name__)

DEFAULT_TRANSFER_DIMENSION = 1024 * 1024  # Bytes
DEFAULT_HTTP_TRANSFER_DIMENSION = 260 + 9437184 + 2  # Bytes (header + file) 16576504
DEFAULT_TEST_DURATION = 10  # seconds
DEFAULT_BT_TRANSFER_DIMENSION = 16397  # Bytes
BITTORRENT_PORT = 6881
BITTORRENT_ALTERNATIVE_PORT = 51413
BITTORRENT_REQUEST_LENGTH = 13
BITTORRENT_REQUEST_TOTAL_LENGTH = 17
BITTORRENT_REQUEST_TYPE = 0x6
BITTORRENT_BLOCK_DIMENSION = 0x4000
BITTORRENT_PIECE_DIMENSION = 0x20000
BITTORRENT_START_INDEX = 0x0
BITTORRENT_START_OFFSET = 0x0
BITTORRENT_RESPONSE_LENGTH = 0x9
BITTORRENT_PIECE_TYPE = 0x7
NUMBER_OF_REQUESTS = 80


class Test(object):
    __metaclass__ = ABCMeta

    def __init__(self, transfer=DEFAULT_TRANSFER_DIMENSION):
        self.transfer_dimension = transfer

    @abstractmethod
    def send_on_socket(self, send_socket, data):
        pass

    @abstractmethod
    def receive_from_socket(self, receive_socket, length):
        pass

    @abstractmethod
    def uplink_test(self, send_socket, duration=DEFAULT_TEST_DURATION):
        pass

    @abstractmethod
    def downlink_test(self, receive_socket, intervals):
        pass


class TCPTest(Test):
    __metaclass__ = ABCMeta

    def __init__(self, transfer=DEFAULT_TRANSFER_DIMENSION):
        Test.__init__(self, transfer)

    def send_on_socket(self, send_socket, data):
        to_send = len(data)
        i = 0
        while to_send != 0:
            sent = send_socket.send(data[i:])
            if sent == 0:
                break
            i += sent
            to_send -= sent

    def receive_from_socket(self, receive_socket, length, intervals=None):
        rec = ""
        while length > 0:
            msg = receive_socket.recv(length)
            if not msg:
                logger.warning("Test: Receiving nothing, connection broken")
                break
            if intervals is not None:
                intervals[time.time()] = len(msg)
            length -= len(msg)
            rec += msg
        return rec

    @staticmethod
    def generate_random_bytes(n):
        rest = n % 4
        number = int(n / 4)
        string = ""
        for i in range(number):
            r = random.getrandbits(32)
            string += struct.pack("!I", r)
        if rest != 0:
            for i in range(rest):
                r = random.getrandbits(8)
                string += struct.pack("!B", r)
        return string


class TCPHTTPTest(TCPTest):
    def __init__(self, transfer_dimension=DEFAULT_HTTP_TRANSFER_DIMENSION):
        TCPTest.__init__(self, transfer_dimension)

    def build_body_response(self):
        piece = self.generate_random_bytes(147457)
        random_byte = ""
        for i in range(0, 32):
            random_byte += piece
        random_byte += self.generate_random_bytes(1)
        random_byte += random_byte
        return random_byte

    def downlink_test(self, receive_socket, intervals):
        request, length = http_server.TestHttpServer.make_request()
        self.send_on_socket(receive_socket, request)
        start = time.time()
        intervals[start] = 0
        while True:
            msg = self.receive_from_socket(receive_socket, self.transfer_dimension, intervals)
            if len(msg) == 5:
                break
        stop = time.time() - 5
        interval = stop - start
        total_rec = len(msg)
        logger.info("Received: %i, Interval: %f, Throughput: %f" % (total_rec, interval, (total_rec / interval)))
        return intervals

    def uplink_test(self, send_socket, duration=DEFAULT_TEST_DURATION):
        request, length = http_server.TestHttpServer.make_request()
        req = self.receive_from_socket(send_socket, length)
        logger.info("Request received: \n" + req)
        response = http_server.TestHttpServer.make_header_response()
        logger.info("Response: \n" + response)
        random_byte = self.build_body_response()
        stop = start = time.time()
        while stop - start < duration:
#per 10 secondi manda byte casuali, poi ne manda uno lungo 5 byte per fase finita
            random_byte = self.generate_random_bytes(9437250)
           	#la riga sopra su random_byte era commentata
	    print "", len(response + "<html><body>" + random_byte + "</body></html>")
            self.send_on_socket(send_socket, response+"<html><body>"+random_byte+"</body></html>")
            stop = time.time()
        logger.info("Uplink Test HTTP: Finished Upload")
        choke = self.generate_random_bytes(5)
        self.send_on_socket(send_socket, choke)


class TCPRandomTest(TCPTest):
    def __init__(self, transfer_dimension=DEFAULT_BT_TRANSFER_DIMENSION):
        Test.__init__(self, transfer_dimension)
        self.random_bytes_response = os.urandom(transfer_dimension * 1000)
        self.offset_response = 0
        self.random_bytes_request = os.urandom(BITTORRENT_REQUEST_TOTAL_LENGTH * NUMBER_OF_REQUESTS * 100)
        self.offset_request = 0

    def build_request(self):
        string = self.random_bytes_request[self.offset_request:self.offset_request + (BITTORRENT_REQUEST_TOTAL_LENGTH *
                                                                                      NUMBER_OF_REQUESTS)]
        self.offset_request += (BITTORRENT_REQUEST_TOTAL_LENGTH * NUMBER_OF_REQUESTS)
        if self.offset_request == len(self.random_bytes_request):
            self.offset_request = 0
        return string

    def build_response(self):
        string = ""
        for i in range(NUMBER_OF_REQUESTS):
            string += self.random_bytes_response[self.offset_response:self.offset_response +
                                                 DEFAULT_BT_TRANSFER_DIMENSION]
            self.offset_response += DEFAULT_BT_TRANSFER_DIMENSION
            if self.offset_response == len(self.random_bytes_response):
                self.offset_response = 0
        return string

    def __uplink_preparation(self, send_socket):
        self.receive_from_socket(send_socket, 68)
        handshake_send = self.generate_random_bytes(68)
        self.send_on_socket(send_socket, handshake_send)
        unchoke = self.generate_random_bytes(5)
        self.send_on_socket(send_socket, unchoke)
        self.receive_from_socket(send_socket, 5)

    def uplink_test(self, send_socket, duration=DEFAULT_TEST_DURATION):
        self.__uplink_preparation(send_socket)
        bytes_sent = 0
        stop = start = time.time()
        logger.info("Uplink Test CT: Start Upload")
        while stop - start < duration:
            # 80 pieces request
            self.receive_from_socket(send_socket, BITTORRENT_REQUEST_TOTAL_LENGTH * NUMBER_OF_REQUESTS)
            response = self.build_response()
            self.send_on_socket(send_socket, response)
            bytes_sent += len(response)
            stop = time.time()
        logger.info("Uplink Test CT: Finished Upload")
        # stop test
        choke = self.generate_random_bytes(5)
        self.send_on_socket(send_socket, choke)

    def __downlink_preparation(self, receive_socket):
        handshake_send = self.generate_random_bytes(68)
        self.send_on_socket(receive_socket, handshake_send)
        # receive handshake
        self.receive_from_socket(receive_socket, 68)
        # receive unchoke
        self.receive_from_socket(receive_socket, 5)
        # send interest
        interest = self.generate_random_bytes(5)
        self.send_on_socket(receive_socket, interest)

    def downlink_test(self, receive_socket, intervals):
        self.__downlink_preparation(receive_socket)
        total_rec = 0
        start = time.time()
        intervals[start] = 0
        # send request (80 pieces of 0x4000 bytes) and receive response
        # if choke received (5 bytes), stop test
        while True:
            request = self.build_request()
            self.send_on_socket(receive_socket, request)
            rec = self.receive_from_socket(receive_socket, self.transfer_dimension * NUMBER_OF_REQUESTS, intervals)
            total_rec += len(rec)
            if len(rec) == 5:
                break
        stop = time.time() - 5
        interval = stop - start
        logger.info("Received: %i, Interval: %f, Throughput: %f" % (total_rec, interval, (total_rec / interval)))
        return intervals


class TCPBTTest(TCPTest):
    def __init__(self, transfer_dimension=DEFAULT_BT_TRANSFER_DIMENSION):
        Test.__init__(self, transfer_dimension)
        self.random_bytes = os.urandom(BITTORRENT_BLOCK_DIMENSION * 1000)
        self.offset = 0

    @staticmethod
    def build_request(index):
        offset = BITTORRENT_START_OFFSET
        request = ""
        for i in range(NUMBER_OF_REQUESTS):
            msg = struct.pack("!IBIII", BITTORRENT_REQUEST_LENGTH, BITTORRENT_REQUEST_TYPE, index, offset,
                              BITTORRENT_BLOCK_DIMENSION)
            request += msg
            offset += BITTORRENT_BLOCK_DIMENSION
            if offset == BITTORRENT_PIECE_DIMENSION:
                offset = BITTORRENT_START_OFFSET
                index += 0x1
        return index, request

    def generate_random_block(self):
        string = self.random_bytes[self.offset:self.offset + BITTORRENT_BLOCK_DIMENSION]
        self.offset += BITTORRENT_BLOCK_DIMENSION
        if self.offset == len(self.random_bytes):
            self.offset = 0
        return string

    def build_response(self, request):
        start = 0
        msg_len = BITTORRENT_RESPONSE_LENGTH + BITTORRENT_BLOCK_DIMENSION
        response = ""
        for i in range(len(request) / BITTORRENT_REQUEST_TOTAL_LENGTH):
            # Index and Offset positions in request, relatively to the start of the single request
            index = request[start + 5:start + 9]
            offset = request[start + 9:start + 13]
            response += struct.pack("!IB", msg_len, BITTORRENT_PIECE_TYPE) + index + offset +\
                        self.generate_random_block()
            start += BITTORRENT_REQUEST_TOTAL_LENGTH
        return response

    def uplink_test(self, send_socket, duration=DEFAULT_TEST_DURATION):
        self.__uplink_preparation(send_socket)
        bytes_sent = 0
        stop = start = time.time()
        logger.info("START BT UPLOADING")
        while stop - start < duration:
            # 80 pieces request
            request = self.receive_from_socket(send_socket, 1360)
            response = self.build_response(request)
            self.send_on_socket(send_socket, response)
            bytes_sent += len(response)
            stop = time.time()
        # stop test
        choke = bytearray.fromhex("0000000100")
        self.send_on_socket(send_socket, choke)


    def __uplink_preparation(self, send_socket):
        logger.info("UPLINK BT PREPARATION")
        self.receive_from_socket(send_socket, 68)
        handshake_send = bytearray.fromhex("13426974546f7272656e742070726f746f636f6c000000000000000031420a403f2ea" +
                                           "41c67aca80b46e956389a7f17b62d5452323832302d36333065666467316a677937")
        self.send_on_socket(send_socket, handshake_send)
        unchoke = bytearray.fromhex("0000000101")
        self.send_on_socket(send_socket, unchoke)
        self.receive_from_socket(send_socket, 5)

    def downlink_test(self, receive_socket, intervals):
        self.__downlink_preparation(receive_socket)
        index = 0x0
        total_rec = 0
        start = time.time()
        intervals[start] = 0
        # send request (80 pieces of 0x4000 bytes) and receive response
        # if choke received (5 bytes), stop test
        while True:
            index, requests = self.build_request(index)
            self.send_on_socket(receive_socket, requests)
            rec = self.receive_from_socket(receive_socket, self.transfer_dimension * NUMBER_OF_REQUESTS, intervals)
            total_rec += len(rec)
            if len(rec) == 5:
                break
        stop = time.time() - 5
        interval = stop - start
        logger.info("Received: %i, Interval: %f, Throughput: %f" % (total_rec, interval, (total_rec / interval)))
        return intervals

    def __downlink_preparation(self, receive_socket):
        handshake_send = bytearray.fromhex("13426974546f7272656e742070726f746f636f6c000000000000000031420a403f2ea" +
                                           "41c67aca80b46e956389a7f17b62d5452323832302d676b36317669687a6d623033")
        self.send_on_socket(receive_socket, handshake_send)
        # receive handshake
        self.receive_from_socket(receive_socket, 68)
        # receive unchoke
        self.receive_from_socket(receive_socket, 5)
        # send interest
        interest = bytearray.fromhex("0000000102")
        self.send_on_socket(receive_socket, interest)
