import socket
import logging
import time
from handlers import ListenerException

logger = logging.getLogger(__name__)

HTTP_PROTOCOL = "HTTP/"
HOST_PREFIX = "Host: "
WHITE_SPACE = " "
TERMINATOR = "\r\n"
OK_RESPONSE = "200 OK"
SERVER = "NeutMon/1.0 (Win64)"
DEFAULT_HTTP_VER = "1.1"


class TestHttpServer:

    def __init__(self, ip="127.0.0.1", port=80):
        try:
            self.ip = ip
            self.port = port
            self.listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.listening_socket.bind((self.ip, self.port))
            self.listening_socket.listen(5)
        except socket.error:
            raise ListenerException("Couldn't initialize listener socket")

    def start_server(self):
        try:
            (client_socket, address) = self.listening_socket.accept()
        except socket.error:
            raise ListenerException("Error while accepting incoming connection")
        return client_socket, address

    def close_socket(self):
        self.listening_socket.close()

    @staticmethod
    def rcv_request(receive_socket, length):
        rec = ""
        while length > 0:
            try:
                msg = receive_socket.recv(length)
                if not msg:
                    logger.warning("Test: Receiving nothing, connection broken")
                    break
                length -= len(msg)
                rec += msg
            except socket.timeout as to:
                logger.info("Timeout occurred, measurement finished: %s" % to.message)
                break
        return rec

    @staticmethod
    def parse_request(request):
        return request.split("\r\n")

    @staticmethod
    def make_request(req_type="GET", path="/index.html", http_ver=DEFAULT_HTTP_VER, host="localhost"):
        request = req_type + WHITE_SPACE + path + WHITE_SPACE + HTTP_PROTOCOL \
                  + http_ver + TERMINATOR + HOST_PREFIX + host + TERMINATOR + TERMINATOR
        length = len(request)
        return request, length

    @staticmethod
    def make_header_response(ver_stat=HTTP_PROTOCOL+WHITE_SPACE+DEFAULT_HTTP_VER + OK_RESPONSE, server=SERVER, last_modify="null", c_len=0,
                             c_type="text/html", conn_stat="Closed"):
        day = time.strftime("%a")
        month = time.strftime("%b")
        day_month = time.strftime("%d")
        year = time.strftime("%Y")
        hour = time.strftime("%H")
        minute = time.strftime("%M")
        seconds = time.strftime("%S")
        date = day + ", " + day_month + " " + month + " " + year + " " + hour + ":" + minute + ":" + seconds + " GMT"
        response = ver_stat + TERMINATOR + \
                   "Date: " + date + TERMINATOR + \
                   "Server: " + server + TERMINATOR + \
                   "Last-Modified: " + last_modify + TERMINATOR + \
                   "Content-Length: " + str(c_len) + TERMINATOR + \
                   "Content-Type: " + c_type + TERMINATOR + \
                   "Connection: " + conn_stat + TERMINATOR + \
                   TERMINATOR
        return response
