from neutmon2 import handlers
from neutmon2 import test

import argparse
import logging
import sys
import time

log_file = "neutmon_client.log"


def main():
    parser = argparse.ArgumentParser(description="NeutMon client. Performs speed and traceroute tests to check if "
                                                 "ISPs are differentiating traffic.")
    parser.add_argument("-s", "--server", help="server address. if not specified server defaults to localhost")
    parser.add_argument("-l", "--log", help="set the logging level. possible values are DEBUG, INFO, WARNING, ERROR,"
                                            "and CRITICAL. if not specified the default value is WARNING")
    parser.add_argument("-v", "--verbose", help="if set logs are also printed on the standard output",
                        action="store_true")

    args = parser.parse_args()
    if args.log:
        if args.log == "DEBUG":
            log_level = logging.DEBUG
        elif args.log == "INFO":
            log_level = logging.INFO
        elif args.log == "WARNING":
            log_level = logging.WARNING
        elif args.log == "ERROR":
            log_level = logging.ERROR
        elif args.log == "CRITICAL":
            log_level = logging.CRITICAL
        else:
            log_level = logging.WARNING
    else:
        log_level = logging.WARNING
    logger = logging.getLogger("neutmon")
    logger.setLevel(logging.DEBUG)
    log_formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] \t%(message)s')
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)
    if args.verbose:
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(log_formatter)
        logger.addHandler(console_handler)
    logger.info("Neutmon client started")
    if args.server:
        server_address = args.server
    else:
        server_address = handlers.DEFAULT_SERVER_ADDRESS
    bt_test = test.TCPBTTest()
    ct_test = test.TCPRandomTest()
    ht_test = test.TCPHTTPTest()
    logger.info("Initializing control connection to server")
    connector = handlers.Connector()
    connector.connect(server_address, handlers.SERVER_PORT)
    controller = handlers.Controller(connector.connector_socket, handlers.ROLE_CLIENT)
    msg = None
    port = None
    phase = None
    test_var = None
    while True:
        try:
            msg, port = controller.recv_control_msg()
        except handlers.ControllerException as ce:
            logger.critical(" Controller error, exiting: %s" % ce.message)
        if msg == handlers.CONTROLLER_ABORT_MEASURE_MSG:
            logger.info("Received abort measure message")
            break
        elif msg == handlers.CONTROLLER_FINISH_MEASURE_MSG:
            logger.info("Received finish measure message")
            break
        elif msg == handlers.CONTROLLER_START_UB_MSG:
            if port is None:
                logger.error("Error: port is None")
                continue
            logger.info("Received message start UB, port %i" % port)
            test_var = bt_test
            phase = handlers.TEST_UPLINK_PHASE
        elif msg == handlers.CONTROLLER_START_UC_MSG:
            if port is None:
                logger.error("Error: port is None")
                continue
            logger.info("Received message start UC, port %i" % port)
            test_var = ct_test
            phase = handlers.TEST_UPLINK_PHASE
        elif msg == handlers.CONTROLLER_START_UH_MSG:
            if port is None:
                logger.error("Error: port is None")
                continue
            logger.info("Received message start UH, port %i" % port)
            test_var = ht_test
            phase = handlers.TEST_UPLINK_PHASE
        elif msg == handlers.CONTROLLER_START_DB_MSG:
            if port is None:
                logger.error("Error: port is None")
                continue
            logger.info("Received message start DB, port %i" % port)
            test_var = bt_test
            phase = handlers.TEST_DOWNLINK_PHASE
        elif msg == handlers.CONTROLLER_START_DC_MSG:
            if port is None:
                logger.error("Error: port is None")
                continue
            logger.info("Received message start DC, port %i" % port)
            test_var = ct_test
            phase = handlers.TEST_DOWNLINK_PHASE
        elif msg == handlers.CONTROLLER_START_DH_MSG:
            if port is None:
                logger.error("Error: port is None")
                continue
            logger.info("Received message start DH, port %i" % port)
            test_var = ht_test
            phase = handlers.TEST_DOWNLINK_PHASE
        elif msg == handlers.CONTROLLER_SEND_META_DATA_MSG:
            logger.info("Received message send meta data")
            meta_data = dict()
            # TODO meta data structure
            logger.info("Metadata: %s" % meta_data)
            logger.info("Sending data to server")
            controller.send_control_msg(handlers.CONTROLLER_OK_MSG, meta_data)
            continue
        try:
            result = dict()
            logger.info("Instantiate tester")
            tester = handlers.Tester(port, handlers.ROLE_CLIENT)
            try:
                logger.info("Connecting tester to server")
                tester.connect(server_address)
                logger.info("Starting test")
                logger.info("Starting test %i" % handlers.TEST_SPEEDTEST_TYPE)
                tester.do_test(test_var, phase, handlers.TEST_SPEEDTEST_TYPE, result)
                if phase == handlers.TEST_UPLINK_PHASE:
                    logger.info("Sleeping")
                    time.sleep(10)
                logger.info("Sending result to server")
                controller.send_control_msg(handlers.CONTROLLER_OK_MSG, result)
            except handlers.TesterException as test_exc:
                if test_exc.errno is None:
                    logger.error("Test failed %s, %i" % (test_exc.message, test_exc.error))
                else:
                    logger.error("Test failed %s, %i, %i" % (test_exc.message, test_exc.error, test_exc.errno))
                if test_exc.error == handlers.TESTER_CONNECT_REFUSED_ERROR:
                    controller.send_control_msg(handlers.CONTROLLER_CLIENT_CONNECT_REFUSED_ERROR)
                elif test_exc.error == handlers.TESTER_CONNECT_GENERIC_ERROR:
                    controller.send_control_msg(handlers.CONTROLLER_CLIENT_CONNECT_GENERIC_ERROR)
                elif test_exc.error == handlers.TESTER_TEST_RESET_ERROR:
                    controller.send_control_msg(handlers.CONTROLLER_CLIENT_TEST_RESET_ERROR, result)
                elif test_exc.error == handlers.TESTER_TEST_ABORT_ERROR:
                    controller.send_control_msg(handlers.CONTROLLER_CLIENT_TEST_ABORT_ERROR, result)
                elif test_exc.error == handlers.TESTER_TEST_GENERIC_ERROR:
                    controller.send_control_msg(handlers.CONTROLLER_CLIENT_TEST_GENERIC_ERROR, result)
            finally:
                logger.info("Closing test connection")
                tester.close_test_connection()
        except handlers.TesterException as te:
            if te.errno is None:
                logger.error("Test failed %s, %i" % (te.message, te.error))
            else:
                logger.error("Test failed %s, %i, %i" % (te.message, te.error, te.errno))
            if te.error == handlers.TESTER_INIT_CLIENT_ERROR:
                controller.send_control_msg(handlers.CONTROLLER_CLIENT_TEST_INIT_ERROR)


if __name__ == "__main__":
    main()
