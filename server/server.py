from neutmon2 import handlers
from neutmon2 import test

import argparse
import sys
import logging
import uuid
import time
import json
import traceback

log_file = "neutmon_server.log"


class Client(object):
    def __init__(self, control_socket, address, cid):
        self.control_socket = control_socket
        self.address = address
        self.id = cid

    def close_connection(self):
        self.control_socket.close()


def init_current_test(port):
    current_test = dict()
    current_test["port"] = port
    current_test["finished"] = False
    current_test["uplink"] = dict()
    current_test["uplink"]["bt"] = dict()
    current_test["uplink"]["ct"] = dict()
    current_test["uplink"]["ht"] = dict()
    current_test["downlink"] = dict()
    current_test["downlink"]["bt"] = dict()
    current_test["downlink"]["ct"] = dict()
    current_test["downlink"]["ht"] = dict()
    return current_test


def client_handler(client, meta_data, results, error, logger):
    logger.info("C: Initializing controller")
    controller = handlers.Controller(client.control_socket)
    bt_test = test.TCPBTTest()
    ct_test = test.TCPRandomTest()
    ht_test = test.TCPHTTPTest()
    port = handlers.BT_PORT
    ht_port = handlers.HT_PORT
    current_test = init_current_test(port)
    results.append(current_test)
    tester = None
    result = None
    try:
        logger.info("C: Initializing Tester")
        tester = handlers.Tester(port)
        logger.info("C: Done")
        for command in range(handlers.CONTROLLER_START_UB_MSG, handlers.CONTROLLER_START_DH_MSG + 1):
            if command == handlers.CONTROLLER_START_UB_MSG or \
               command == handlers.CONTROLLER_START_UH_MSG or \
               command == handlers.CONTROLLER_START_UC_MSG:
                phase = handlers.TEST_DOWNLINK_PHASE
                phase_index = "uplink"
            else:
                phase = handlers.TEST_UPLINK_PHASE
                phase_index = "downlink"
            if command == handlers.CONTROLLER_START_UB_MSG or command == handlers.CONTROLLER_START_DB_MSG:
                test_var = bt_test
                test_index = "bt"
            elif command == handlers.CONTROLLER_START_UC_MSG or command == handlers.CONTROLLER_START_DC_MSG:
                test_var = ct_test
                test_index = "ct"
            else:
                test_var = ht_test
                test_index = "ht"
            logger.info("C: Sending control message %i port %i" % (command, port))
            controller.send_control_msg(command, port)
            try:
                logger.info("C: Doing test")
                result = dict()
                tester.accept_test_connection()
                logger.info("Starting %i test, phase %s %s" % (handlers.TEST_SPEEDTEST_TYPE, test_index, phase_index))

                tester.do_test(test_var, phase, handlers.TEST_SPEEDTEST_TYPE, result)
                current_test[phase_index][test_index]["server_status"] = handlers.TESTER_OK
                if phase == handlers.TEST_UPLINK_PHASE:
                    logger.info("C: Sleeping")
                    time.sleep(10)
                logger.info("C: Closing test connection")

                tester.close_test_connection()
            except handlers.TesterException as te:
                current_test[phase_index][test_index]["server_status"] = te.error
                if te.errno is None:
                    logger.error("C: Error in test %s %s on port %i: %s" % (phase_index, test_index, port, te.message))
                else:
                    logger.error("C: Error in test %s %s on port %i: %s %i" % (phase_index, test_index, port, te.message, te.errno))
                tester.close_test_connection()
            if phase == handlers.TEST_DOWNLINK_PHASE:
                current_test[phase_index][test_index]["speedtest"] = result
            logger.info("C: Receiving status and result from client")
            resp, extra = controller.recv_control_msg()
            logger.info("C: Client status is %i" % resp)
            current_test[phase_index][test_index]["client_status"] = resp
            if extra is not None:
                logger.info("C: client result is not empty")
                if phase == handlers.TEST_UPLINK_PHASE:
                    current_test[phase_index][test_index]["speedtest"] = extra
        current_test["finished"] = True
        logger.info("C: Finishing test and closing test connection")
        tester.finish_test()
        logger.info("C: Sending control message send meta data")
        controller.send_control_msg(handlers.CONTROLLER_SEND_META_DATA_MSG)
        logger.info("C: Receiving status and result from client")
        resp, extra = controller.recv_control_msg()
        if resp == handlers.CONTROLLER_OK_MSG and extra is not None:
            meta_data["client_meta"] = extra
        else:
            logger.warning("C: meta data not received")
            meta_data["client_meta"] = {}
        controller.finish_measure()
    except handlers.ControllerException as ce:
        logger.error("C: Error in controller: %s" % ce.message)
        error["message"] = ce.message
        tester.finish_test()
        try:
            controller.abort_measure()
        except handlers.ControllerException:
            pass
    except handlers.TesterException as te:
        logger.error("C: Error in tester: %s %i %i" % (te.message, te.error, te.errno))
        error["message"] = te.error
        tester.finish_test()
        try:
            controller.abort_measure()
        except handlers.ControllerException:
            pass
    except Exception as e:
        error["message"] = "%s: %s" % (type(e).__name__, e.message)
        logger.error("C: Unexpected error %s %s %s" % (type(e).__name__, e.message, e.args))
        logger.error(traceback.format_exc())
        tester.finish_test()
        try:
            controller.abort_measure()
        except handlers.ControllerException:
            pass
    finally:
        client.close_connection()
    pass


def main():
    parser = argparse.ArgumentParser(description="NeutMon client. Performs speed tests to check if "
                                                 "ISPs are differentiating traffic.")
    parser.add_argument("-l", "--log", help="set the logging level. possible values are DEBUG, INFO, WARNING, ERROR,"
                                            "and CRITICAL. if not specified the default value is WARNING")
    parser.add_argument("-v", "--verbose", help="if set logs are also printed on the standard output",
                        action="store_true")
    args = parser.parse_args()
    if args.log:
        if args.log == "DEBUG":
            log_level = logging.DEBUG
        elif args.log == "INFO":
            log_level = logging.INFO
        elif args.log == "WARNING":
            log_level = logging.WARNING
        elif args.log == "ERROR":
            log_level = logging.ERROR
        elif args.log == "CRITICAL":
            log_level = logging.CRITICAL
        else:
            log_level = logging.WARNING
    else:
        log_level = logging.WARNING
    logger = logging.getLogger("neutmon")
    logger.setLevel(log_level)
    log_formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] \t%(message)s')
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)
    if args.verbose:
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(log_formatter)
        logger.addHandler(console_handler)
    logger.info("Neutmon server started")
    logger.info("P: Initializing listener")
    listener = handlers.Listener()
    while True:
        logger.info("P: Accepting incoming connection")
        client_socket, address = listener.accept_connection()
        client_id = str(uuid.uuid4())
        client = Client(client_socket, address, client_id)
        meta_data = dict()
        error = dict()
        results = []
        meta_data["client_id"] = client_id
        meta_data["client_ip"] = address
        meta_data["start"] = time.time()
        logger.info("P: Passing client connection to handler")
        client_handler(client, meta_data, results, error, logger)
        meta_data["stop"] = time.time()
        result = dict()
        result["meta_data"] = meta_data
        result["results"] = results
        if error:
            result["error"] = error
        logger.info("P: Writing results on file")
        with open("output-" + str(int(time.time())) + "-" + client_id + ".json", "w") as f:
            f.write(json.dumps(result, indent=4))


if __name__ == "__main__":
    main()
