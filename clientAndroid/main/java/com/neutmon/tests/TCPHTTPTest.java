package com.neutmon.tests;

import android.util.Log;

import com.neutmon.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Dictionary;


public class TCPHTTPTest extends TCPTest {

    private static final String HTTP_PROTOCOL = "HTTP/";
    private static final String HOST_PREFIX = "Host: ";
    private static final String WHITE_SPACE = " ";
    private static final String TERMINATOR = "\r\n";
    private static final String OK_RESPONSE = "200 OK";
    private static final String SERVER = "Apache/1.1 (Win64)";
    private static final String DEFAULT_HTTP_VER = "1.1";
    private static final String TAG = "TCPHTTPTest";

    public TCPHTTPTest(int transferDim) {
        super(transferDim);
    }

    private byte[] buildBodyResponse() throws IOException{
        String response = new String(makeHeaderResponse());
        String p1 = "<html><body>";
        String p2 = "</body></html>";
        int bytes = (response+p1+p2).getBytes().length;
        byte[] body = generateRandomBytes(Constants.DEFAULT_HTTP_TRANSFER_DIMENSION - bytes);
        ByteArrayOutputStream randomByte = new ByteArrayOutputStream();
        randomByte.write(response.getBytes());
        randomByte.write(p1.getBytes());
        randomByte.write(body);
        randomByte.write(p2.getBytes());
        /*
        byte[] piece = generateRandomBytes(147457);
        ByteArrayOutputStream randomByte = new ByteArrayOutputStream();
        for(int i=0; i<32; i++)
            randomByte.write(ByteBuffer.allocate(piece.length).put(piece).array());
        byte[] middle_byte = generateRandomBytes(1);
        randomByte.write(middle_byte[0]);
        for(int i=0; i<32; i++)
            randomByte.write(ByteBuffer.allocate(piece.length).put(piece).array());
        */
        return randomByte.toByteArray();
    }

    private byte[] makeHttpRequest(String host){
        String request = "GET" + WHITE_SPACE + "/index.html" + WHITE_SPACE + HTTP_PROTOCOL
                + DEFAULT_HTTP_VER + TERMINATOR + HOST_PREFIX + host + TERMINATOR + TERMINATOR;
        return request.getBytes();
    }

    private byte[] makeHeaderResponse(){
        String response = HTTP_PROTOCOL+WHITE_SPACE+DEFAULT_HTTP_VER + OK_RESPONSE + TERMINATOR +
                "Date: " + "Mon, 21 May 2018 15:52:30 GMT" + TERMINATOR +
                "Server: " + SERVER + TERMINATOR +
                "Last-Modified: " + "" + TERMINATOR +
                "Content-Length: " + "" + TERMINATOR +
                "Content-Type: " + "" + TERMINATOR +
                "Connection: " + "closed" +
                TERMINATOR +
                TERMINATOR;
        return response.getBytes();
    }

    @Override
    public void uplinkTest(Socket sendSocket, int duration) throws Exception {
        byte[] request = makeHttpRequest("localhost");
        receiveFromSocket(sendSocket, request.length);
        //String response = new String(makeHeaderResponse());
        byte[] randomByte = buildBodyResponse();
        //Log.d(TAG, "Random byte len: "+randomByte.length);
        //String Srb = new String(randomByte, "UTF-8");
        long start = System.currentTimeMillis() / 1000L;
        long stop = start;
        if(duration == 0)
            duration = Constants.DEFAULT_TEST_DURATION;
        Log.d(TAG,"HTTP Uplink starting");
        while (stop - start < duration) {
            Log.d(TAG, "len: " + randomByte.length);
            sendOnSocket(sendSocket, randomByte);
            stop = System.currentTimeMillis() / 1000L;
        }
        byte[] choke = generateRandomBytes(5);
        sendOnSocket(sendSocket, choke);
        Log.d(TAG,"HTTP Uplink test ended");
    }

    @Override
    public void downlinkTest(Socket receiveSocket, Dictionary<String, Integer> intervals) throws Exception {
        byte[] request = makeHttpRequest("localhost");
        //Log.d(TAG, "Request len: "+ request.length);
        sendOnSocket(receiveSocket, request);
        long start = System.currentTimeMillis();
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(3);
        df.setGroupingUsed(false);
        intervals.put(df.format((double)start / 1000d).replace(',', '.'), 0);
        int i=0;
        while(true){
            byte[] msg = receiveFromSocket(receiveSocket, transferDim, intervals);
            //i++;
            Log.d(TAG, "MSG :"+msg.length);
            if (msg.length == 5) {
                Log.d(TAG, "HTTP Downlink test ended");
                break;
            }
            if(i==5)
                break;
        }
    }
}
