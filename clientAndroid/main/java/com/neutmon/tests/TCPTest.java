package com.neutmon.tests;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.neutmon.Constants;
import com.neutmon.exceptions.TesterException;


public abstract class TCPTest extends Test {

    private static final String TAG = "TCPTest";

    public TCPTest(int transferDim) {
        super(transferDim);
    }

    @Override
    public void sendOnSocket(Socket sendSocket, byte[] data) throws TesterException {
        try {
            OutputStream outStream = sendSocket.getOutputStream();
            double start = System.currentTimeMillis();
            outStream.write(data);
            double end = System.currentTimeMillis();
            //TODO manage time get
            uploadedDataRegister.addData(start, end, data.length);
        }catch (IOException e){
            throw new TesterException(
                    Constants.TESTER_TEST_GENERIC_ERROR,
                    "Error in sending data. " + e.getMessage()
            );
        }
    }

    @Override
    public byte[] receiveFromSocket(Socket receiveSocket, int length) throws TesterException {
        return receiveFromSocket(receiveSocket, length, null);
    }

    @Override
    public byte[] receiveFromSocket(Socket receiveSocket, int length, Dictionary<String, Integer> intervals) throws TesterException {
        byte[] inBytes = null;
        int receivedBytes = 0;
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(3);
        df.setGroupingUsed(false);
        int oldRB = 0;
        try {
            inBytes = ByteBuffer.allocate(length).array();
            InputStream inStream = receiveSocket.getInputStream();
            while (length != 0) {
                double start = System.currentTimeMillis();
                receivedBytes = inStream.read(inBytes, inBytes.length - length, length);
                if(receivedBytes == -1 && oldRB == 5) {
                    inBytes = Arrays.copyOfRange(inBytes, 0, 5);
                    break;
                }
                oldRB = receivedBytes;
                if (receivedBytes == -1) {
                    Log.d(TAG, "Error in receiving data");
                    break;
                }
                double end = System.currentTimeMillis();
                downloadedDataRegister.addData(start, end, receivedBytes);
                //TODO manage time get
                length -= receivedBytes;
            }
        } catch (IOException e) {
            if(receivedBytes == 5)
                inBytes = Arrays.copyOfRange(inBytes, 0, 5);
            else {
                throw new TesterException(
                        Constants.TESTER_TEST_GENERIC_ERROR,
                        "Error in receiving data. " + e.getMessage()
                );
            }
        }
        if (intervals != null) {
            //TODO manage Intervals
            //Write inside intervals the values that have to be sent to the server
            HashMap<Double, Integer> hashMap = downloadedDataRegister.getIntervals();
            Set<Map.Entry<Double, Integer>> set = hashMap.entrySet();
            for (Map.Entry<Double, Integer> entry : set) {
                intervals.put(
                        df.format(entry.getKey() / 1000d).replace(",", "."),
                        entry.getValue()
                );
            }
        }
        return inBytes;
    }
    public byte[] receiveFromSocket2(Socket receiveSocket, int length, Dictionary<String, Integer> intervals) throws TesterException {
        byte[] inBytes = null;
        int receivedBytes = 0;
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(3);//il dato che vedo in System.out ha tre cifre decimali. Accanto sta indicata la lunghezza
        df.setGroupingUsed(false);
        int oldRB = 0;
        int volte = 0;
        try {
            inBytes = ByteBuffer.allocate(length).array();
            InputStream inStream = receiveSocket.getInputStream();
            while (length != 0) {
                volte ++;
                double start = System.currentTimeMillis();
                receivedBytes = inStream.read(inBytes, inBytes.length - length, length);
                if(receivedBytes == -1 && oldRB == 5) {
                    inBytes = Arrays.copyOfRange(inBytes, 0, 5);
                    break;
                }
                oldRB = receivedBytes;
                Log.d(TAG,"receivedBytes: " + receivedBytes + "length: "+ length + "volte fatte: " + volte);
                if (receivedBytes == -1) {
                    Log.d(TAG, "Error in receiving data");
                    break;
                }
                double end = System.currentTimeMillis();
                downloadedDataRegister.addData(start, end, receivedBytes);
                //TODO manage time get
                length -= receivedBytes;
                if(volte==40)
                 break;
            }
        } catch (IOException e) {
            if(receivedBytes == 5)
                inBytes = Arrays.copyOfRange(inBytes, 0, 5);
            else {
                throw new TesterException(
                        Constants.TESTER_TEST_GENERIC_ERROR,
                        "Error in receiving data. " + e.getMessage()
                );
            }
        }
        if (intervals != null) {
            //TODO manage Intervals
            //Write inside intervals the values that have to be sent to the server
            HashMap<Double, Integer> hashMap = downloadedDataRegister.getIntervals();
            Set<Map.Entry<Double, Integer>> set = hashMap.entrySet();
            for (Map.Entry<Double, Integer> entry : set) {
                intervals.put(
                        df.format(entry.getKey() / 1000d).replace(",", "."),
                        entry.getValue()
                );
            }
        }
        return inBytes;
    }

    public byte[] generateRandomBytes(int n) {
        byte[] randBytes = new byte[n];
        new Random().nextBytes(randBytes);
        return  randBytes;
    }
}
