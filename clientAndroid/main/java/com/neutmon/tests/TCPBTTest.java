package com.neutmon.tests;

import android.util.Log;

import com.neutmon.Constants;
import com.neutmon.exceptions.TesterException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Random;


public class TCPBTTest extends TCPTest {

    private byte[] randomBytes;
    private int offset;
    private static final String TAG = "TCPBTTest";

    public TCPBTTest(int transferDim) {
        super(transferDim);
        randomBytes = new byte[Constants.BITTORRENT_BLOCK_DIMENSION * 1000];
        new Random().nextBytes(randomBytes);
        offset = 0;
    }

    private class BuildReqResult {
        byte[] request;
        int index;

        BuildReqResult(byte[] b, int i) {
            request = b;
            index = i;
        }
    }

    private BuildReqResult buildRequest(int index) throws IOException {
        int offset = Constants.BITTORRENT_START_OFFSET;
        ByteArrayOutputStream reqOutStream = new ByteArrayOutputStream();
        for (int i = 0; i < Constants.NUMBER_OF_REQUESTS; i++) {
            ByteBuffer header = ByteBuffer.allocate(17)
                    .putInt(Constants.BITTORRENT_REQUEST_LENGTH)
                    .put(Constants.BITTORRENT_REQUEST_TYPE)
                    .putInt(index)
                    .putInt(offset)
                    .putInt(Constants.BITTORRENT_BLOCK_DIMENSION);
            reqOutStream.write(header.array());

            offset += Constants.BITTORRENT_BLOCK_DIMENSION;
            if (offset == Constants.BITTORRENT_PIECE_DIMENSION) {
                offset = Constants.BITTORRENT_START_OFFSET;
                index += 0x1;
            }
        }
        return new BuildReqResult(reqOutStream.toByteArray(), index);
    }

    private byte[] generateRandomBlock() {
        byte[] block = Arrays.copyOfRange(
                randomBytes,
                offset,
                offset + Constants.BITTORRENT_BLOCK_DIMENSION
        );
        offset += Constants.BITTORRENT_BLOCK_DIMENSION;
        if (offset >= randomBytes.length) {
            offset = 0;
        }
        return block;
    }

    private byte[] buildResponse(byte[] request) throws IOException {
        int start = 0;
        int msgLen = Constants.BITTORRENT_RESPONSE_LENGTH + Constants.BITTORRENT_BLOCK_DIMENSION;
        ByteArrayOutputStream resp = new ByteArrayOutputStream();
        for (int i = 0; i < request.length / Constants.BITTORRENT_REQUEST_TOTAL_LENGTH; i++) {
            //Index and Offset positions in request,
            //relatively to the start of the single request
            byte[] index = Arrays.copyOfRange(request, start + 5, start + 9);
            byte[] offset = Arrays.copyOfRange(request, start + 9, start + 13);
            resp.write(ByteBuffer.allocate(4).putInt(msgLen).array());
            resp.write(ByteBuffer.allocate(1).put(Constants.BITTORRENT_PIECE_TYPE).array());
            resp.write(index);
            resp.write(offset);
            resp.write(generateRandomBlock());
            start += Constants.BITTORRENT_REQUEST_TOTAL_LENGTH;
        }
        return resp.toByteArray();
    }

    private void uplinkPreparation(Socket sendSocket) throws TesterException {
        byte[] recv = receiveFromSocket(sendSocket, 68);
        byte[] handshakeSend = hexStringToByteArray(
                "13426974546f7272656e742070726f746f636f6c000000000000000031420a403f2ea" +
                        "41c67aca80b46e956389a7f17b62d5452323832302d36333065666467316a677937"
        );
        sendOnSocket(sendSocket, handshakeSend);
        byte[] unchoke = hexStringToByteArray("0000000101");
        sendOnSocket(sendSocket, unchoke);
        receiveFromSocket(sendSocket, 5);
    }

    private static byte[] hexStringToByteArray(String string) {
        int len = string.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte)((Character.digit(string.charAt(i), 16) << 4) +
                    Character.digit(string.charAt(i+1), 16));
        }
        return data;
    }

    @Override
    public void uplinkTest(Socket sendSocket, int duration) throws Exception {
        uplinkPreparation(sendSocket);
        long start, stop;
        start = stop = System.currentTimeMillis() / 1000L;
        //TODO manage time
        //uploadedDataRegister.activate();
        if(duration == 0)
            duration = Constants.DEFAULT_TEST_DURATION;
        while (stop - start < duration) {
            //80 pieces request
            byte[] request = receiveFromSocket(sendSocket, 1360);
            byte[] response = buildResponse(request);
            sendOnSocket(sendSocket, response);
            stop = System.currentTimeMillis() / 1000L;
            //TODO manage Graph
            //displayGraph(uploadedDataRegister, Constants.UPLINK_BITTORRENT_PHASE);
        }
        //uploadedDataRegister.deactivate();
        byte[] choke = hexStringToByteArray("0000000100");
        sendOnSocket(sendSocket, choke);
        Log.d(TAG, "Bittorrent Uplink test ended");
    }

    private void downlinkPreparation(Socket recvSocket) throws TesterException {
        byte[] handshakeSend = hexStringToByteArray(
                "13426974546f7272656e742070726f746f636f6c000000000000000031420a403f2ea" +
                        "41c67aca80b46e956389a7f17b62d5452323832302d676b36317669687a6d623033"
        );
        sendOnSocket(recvSocket, handshakeSend);
        //Receive handshake
        receiveFromSocket(recvSocket, 68);
        //Receive unchoke
        receiveFromSocket(recvSocket, 5);
        //Send interest
        sendOnSocket(recvSocket, hexStringToByteArray("0000000102"));
    }

    @Override
    public void downlinkTest(Socket recvSocket, Dictionary<String, Integer> intervals) throws Exception {
        downlinkPreparation(recvSocket);
        int index = 0x0;
        int totalRec = 0;
        long start = System.currentTimeMillis();
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(3);
        df.setGroupingUsed(false);
        intervals.put(df.format((double)start / 1000d).replace(',', '.'), 0);
        //send request (80 pieces of 0x4000 bytes) and receive response
        //if choke received (5 bytes), stop test
        //TODO manage time
        //downloadedDataRegister.activate();
        while(true) {
            BuildReqResult brr = buildRequest(index);
            index = brr.index;
            sendOnSocket(recvSocket, brr.request);
            byte[] rec = receiveFromSocket(
                    recvSocket, transferDim * Constants.NUMBER_OF_REQUESTS, intervals
            );
            totalRec += rec.length;
            if (rec.length == 5) {
                Log.d(TAG, "Bittorrent Downlink test ended");
                break;
            }
            //TODO manage graph
            //displayGraph(downloadedDataRegister, Constants.DOWNLINK_BITTORRENT_PHASE);
        }
        Log.d(TAG, "BT downlink test DONE");
        //downloadedDataRegister.deactivate();
    }
}
