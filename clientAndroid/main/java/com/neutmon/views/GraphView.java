package com.neutmon.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.neutmon.R;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.Arrays;


public class GraphView extends View {
    private static final int MAX_LABELS = 6;
    private Paint paint;
    private double[] signalStrengthValues;
    private double[] signalStrengthInstants;
    private double[] throughputValues;
    private double[] throughputInstants;
    private String[] horlabels;
    private String[] verlabels;
    private String[] siglabels;
    private String title;
    private String xLabel;
    private String yLabel;
    private String yLabel2;
    private double xMaxValue;
    private Path signalStrength;
    private DashPathEffect dashEffect;
    private int defaultColor;

    public GraphView(Context context, int id, double[] throughputInstants, double[] throughputValues,
                     double[] signalStrengthInstants, double[] signalStrengthValues, String title,
                     String xLabel, String yLabel, String yLabel2, double testDuration) {
        super(context);

        defaultColor = ((TextView)((Activity)context).findViewById(R.id.messages))
                .getTextColors().getDefaultColor();
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(1);
        df.setGroupingUsed(false);

        double maxX;
        if (throughputInstants == null) {
            this.throughputInstants = new double[0];
            maxX = 0d;
        } else {
            this.throughputInstants = throughputInstants;
            if (throughputInstants.length > 0) {
                maxX = Math.ceil(throughputInstants[throughputInstants.length - 1]);
            } else {
                maxX = 0d;
            }
        }
        if (throughputValues == null) {
            this.throughputValues = new double[0];
        } else {
            this.throughputValues = throughputValues;
        }
        if (signalStrengthInstants == null) {
            this.signalStrengthInstants = new double[0];
        } else {
            this.signalStrengthInstants = signalStrengthInstants;
        }
        if (signalStrengthValues == null) {
            this.signalStrengthValues = new double[0];
        } else {
            this.signalStrengthValues = signalStrengthValues;
        }
        //Adjust array lengths
        if (this.throughputValues.length != this.throughputInstants.length) {
            if (this.throughputValues.length > this.throughputInstants.length) {
                this.throughputValues = Arrays.copyOf(
                        this.throughputValues,
                        this.throughputInstants.length
                );
            } else {
                this.throughputInstants = Arrays.copyOf(
                        this.throughputInstants,
                        this.throughputValues.length
                );
            }
        }
        if (this.signalStrengthValues.length != this.signalStrengthInstants.length) {
            if (this.signalStrengthValues.length > this.signalStrengthInstants.length) {
                this.signalStrengthValues = Arrays.copyOf(
                        this.signalStrengthValues,
                        this.signalStrengthInstants.length
                );
            } else {
                this.signalStrengthInstants = Arrays.copyOf(
                        this.signalStrengthInstants,
                        this.signalStrengthValues.length
                );
            }
        }

        if (title == null) {
            title = "";
        } else {
            this.title = title;
        }
        this.xLabel = xLabel;
        this.yLabel = yLabel;
        this.yLabel2 = yLabel2;

        if (testDuration > maxX) {
            this.xMaxValue = testDuration;
        } else {
            this.xMaxValue = maxX;
        }
        //Make horizontal labels
        double increment = xMaxValue / (MAX_LABELS - 1);
        horlabels = new String[MAX_LABELS];
        for (int i = 0; i < MAX_LABELS; i++) {
            horlabels[i] = df.format(i * increment).replace(',', '.');
        }
        //make vertical labels
        double max = Math.ceil(getMax(throughputValues));
        increment = max / (MAX_LABELS - 1);
        verlabels = new String[MAX_LABELS];
        for (int i = 0; i < MAX_LABELS; i++) {
            verlabels[i] = df.format(i * increment).replace(',', '.');
        }
        //make signal strength labels
        siglabels = new String[5];
        for (int i = 0; i < 5; i++) {
            siglabels[i] = String.valueOf(i);
        }

        paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(2f);

        dashEffect = new DashPathEffect(new float[] {10, 7}, 0);
        signalStrength = new Path();
        setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (int)TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        200,
                        getResources().getDisplayMetrics()
                )
        ));
        //Set the ID in order to update the graph as new data arrive
        setId(id);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float marginLeft = 80f;
        float marginTop = 50f;
        float marginBottom = 70f;
        float marginRight = 60f;

        float height = getHeight();
        float width = getWidth();
        float graphHeight = height - marginTop - marginBottom;
        float graphWidth = width - marginLeft - marginRight;

        paint.setTextSize(25);
        paint.setStrokeWidth(1f);
        paint.setColor(defaultColor);

        //Draw vertical labels and markers
        double maxThroughput = Math.ceil(getMax(throughputValues));
        float increment = (float) maxThroughput / (verlabels.length - 1);
        maxThroughput *= 1.02;
        paint.setTextAlign(Align.RIGHT);
        float yScale = (float) (graphHeight / maxThroughput);
        if (throughputValues.length > 0) {
            for (int i = 0; i < verlabels.length; i++) {
                float y = height - marginBottom - (yScale * i * increment);
                canvas.drawLine(marginLeft, y, marginLeft + 10, y, paint);
                canvas.drawText(verlabels[i], marginLeft - 10, y + 10, paint);
            }
        }

        //Draw horizontal labels and markers
        if (horlabels.length > 0) {
            paint.setTextAlign(Align.CENTER);
            int hors = horlabels.length - 1;
            for (int i = 0; i < horlabels.length; i++) {
                float x = ((graphWidth / hors) * i) + marginLeft;
                canvas.drawLine(x, height - marginBottom, x, height - marginBottom - 10, paint);
                canvas.drawText(horlabels[i], x, height - marginBottom + 35, paint);
            }
        }

        double maxSignalStrength = 5 * 1.02;
        if (maxSignalStrength > 0.0) {
            paint.setTextAlign(Align.LEFT);
            yScale = (float) (graphHeight / maxSignalStrength);
            for (int i = 0; i < siglabels.length; i++) {
                float y = height - marginBottom - (yScale * i);
                canvas.drawLine(width - marginRight - 10, y, width - marginRight, y, paint);
                canvas.drawText(siglabels[i], width - marginRight + 10, y + 10, paint);
            }
        }

        //Draw the graph borders
        paint.setStrokeWidth(2f);
        canvas.drawLine(
                marginLeft, height - marginBottom,
                marginLeft + graphWidth,
                height - marginBottom,
                paint
        ); //bottom
        canvas.drawLine(
                marginLeft,
                marginTop,
                marginLeft + graphWidth,
                marginTop,
                paint
        ); //top
        canvas.drawLine(
                marginLeft,
                height - marginBottom,
                marginLeft, marginTop,
                paint
        ); //left
        canvas.drawLine(
                marginLeft + graphWidth,
                height - marginBottom,
                marginLeft + graphWidth,
                marginTop,
                paint
        ); //right

        //Draw x label
        paint.setTextAlign(Align.CENTER);
        canvas.drawText(xLabel, marginLeft + graphWidth / 2, height - 10, paint);
        //Draw y label
        paint.setTextAlign(Align.CENTER);
        paint.setColor(Color.parseColor("#01579b"));
        canvas.save();
        canvas.rotate(-90, 25, graphHeight / 2 + marginTop);
        canvas.drawText(yLabel, 25, graphHeight / 2 + marginTop, paint);
        canvas.restore();
        //Draw the second y label
        paint.setColor(Color.parseColor("#29b6f6"));
        canvas.rotate(-90, width - 10, graphHeight / 2 + marginTop);
        canvas.drawText(yLabel2, width - 10, graphHeight / 2 + marginTop, paint);
        canvas.restore();
        //Draw the graph title
        paint.setColor(defaultColor);
        paint.setTextSize(30);
        canvas.drawText(title, (graphWidth / 2) + marginLeft, 35, paint);

        //Draw the path for the throughput
        float xScale;
        double maxTime = 0.0;
        if (throughputValues.length > 0) {
            yScale = (float)(graphHeight / maxThroughput);
            xScale = (float)(graphWidth / xMaxValue);
            float lastY = 0f;
            float lastX = 0f;
            paint.setColor(Color.parseColor("#01579b"));
            //Draw the lines with a blue circle at each end
            for (int i = 0; i < throughputValues.length; i++) {
                if (throughputInstants[i] > xMaxValue) break;
                maxTime = throughputInstants[i];
                float y = height- marginBottom - (float)throughputValues[i] * yScale;
                float x = marginLeft + (float)throughputInstants[i] * xScale;
                if (i > 0) {
                    canvas.drawLine(lastX, lastY, x, y, paint);
                }
                canvas.drawCircle(x, y, 5, paint);
                lastY = y;
                lastX = x;
            }
            //Draw the white fillings for the blue circles
            paint.setColor(Color.WHITE);
            for (int i = 0; i < throughputValues.length; i++) {
                if (throughputInstants[i] > xMaxValue) break;
                float y = height- marginBottom - (float)throughputValues[i] * yScale;
                float x = marginLeft + (float)throughputInstants[i] * xScale;
                canvas.drawCircle(x, y, 3, paint);
            }
        }
        //Draw the second path for the signal strength
        if (signalStrengthValues.length > 0) {
            yScale = (float)(graphHeight / maxSignalStrength);
            xScale = (float)(graphWidth / xMaxValue);
            float lastY = 0f;
            float lastX = 0f;
            paint.setColor(Color.parseColor("#29b6f6"));
            signalStrength.reset();
            //Draw the lines
            for (int i = 0; i < signalStrengthValues.length; i++) {
                float x = marginLeft + (float)signalStrengthInstants[i] * xScale;
                float y = height- marginBottom - (float)signalStrengthValues[i] * yScale;
                if (signalStrength.isEmpty()) {
                    signalStrength.moveTo(x, y);
                } else {
                    signalStrength.lineTo(x, lastY);
                    signalStrength.lineTo(x, y);
                }
                canvas.drawCircle(x, y, 5, paint);
                lastY = y;
                lastX = x;
            }
            //Make the x-value arrive until the x-value of the throughput
            if (marginLeft + (float)maxTime * xScale > lastX) {
                signalStrength.lineTo(marginLeft + (float)maxTime * xScale, lastY);
            }
            paint.setStyle(Paint.Style.STROKE);
            paint.setPathEffect(dashEffect);
            canvas.drawPath(signalStrength, paint);
            paint.setStyle(Paint.Style.FILL);
            paint.setPathEffect(null);
            //Draw the white fillings for the blue circles
            paint.setColor(Color.WHITE);
            for (int i = 0; i < signalStrengthValues.length; i++) {
                float y = height- marginBottom - (float)signalStrengthValues[i] * yScale;
                float x = marginLeft + (float)signalStrengthInstants[i] * xScale;
                canvas.drawCircle(x, y, 3, paint);
            }
        }
    }

    private double getMax(double[] values) {
        double largest = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < values.length; i++) {
            if (values[i] < 0.0) {
                values[i] = 0.0;
            }
            if (values[i] > largest) {
                largest = values[i];
            }
        }
        return largest;
    }

    private double getMin(double[] values) {
        double smallest = Double.POSITIVE_INFINITY;
        for (int i = 0; i < values.length; i++)
            if (values[i] < smallest)
                smallest = values[i];
        return smallest;
    }

    private double[] concatenate (double[] a, double[] b) {
        int aLen = a.length;
        int bLen = b.length;

        double[] c = (double[])Array.newInstance(a.getClass().getComponentType(), aLen+bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }
}