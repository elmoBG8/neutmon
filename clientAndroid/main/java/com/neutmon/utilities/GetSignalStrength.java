package com.neutmon.utilities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.neutmon.Constants;

public class GetSignalStrength extends AsyncTask {
    Context context;
    @Override
    protected Object doInBackground(Object[] params) {
        context = (Context)params[0];
        int level = Connectivity.getSignalStrength(context);
        return level;
    }

    @Override
    protected void onPostExecute(Object result) {
        int level = (int)result;
        Intent intent = new Intent(Constants.BROADCAST_ACTION_SIGNAL_STRENGTH);
        intent.putExtra(Constants.INTENT_SIGNAL_STRENGTH, level);
        double time = System.currentTimeMillis() / 1000d;
        intent.putExtra(Constants.INTENT_SIGNAL_STRENGTH_INSTANT, time);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
