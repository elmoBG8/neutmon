package com.neutmon.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

public class Connectivity {
    private static final int SIGNAL_STRENGTH_NONE_OR_UNKNOWN = 0;

    public static SignalStrength signalStrength = null;

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static int getSignalStrength(Context context) {
        if (isConnectedWifi(context)) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            return WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 6);
        }
        else if (isConnectedMobile(context) && signalStrength != null) {
            int level;
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                level = signalStrength.getLevel();
            } else {
                level = getLevel();
            }
            //In the new api with SDK >= 23 getLevel() returns also 5 for a good connection
            /*if (level > 4) {
                level = 4;
            }*/
            return level;
        }
        return SIGNAL_STRENGTH_NONE_OR_UNKNOWN ;
    }

    public static String getNetworkOperator(Context context) {
        if (isConnectedWifi(context)) {
            return "WiFi";
        } else if (isConnectedMobile(context)) {
            TelephonyManager telephonyManager =
                    (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                return telephonyManager.getNetworkOperatorName();
            }
        }
        return "No connection";
    }

    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() &&
                info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() &&
                info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public static int getConnectionType(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        int type = info.getType();
        if (type == ConnectivityManager.TYPE_MOBILE) {
            return info.getSubtype();
        }
        return -1;
    }

    public static String getConnectionSubtypeName(int subType) {
        switch (subType) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1xRTT"; // ~ 50-100 kbps
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA"; // ~ 14-64 kbps
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE"; // ~ 50-100 kbps
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EV-DO"; // ~ 400-1000 kbps
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EV-DO Revision A"; // ~ 600-1400 kbps
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS"; // ~ 100 kbps
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA"; // ~ 2-14 Mbps
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA"; // ~ 700-1700 kbps
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA"; // ~ 1-23 Mbps
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS"; // ~ 400-7000 kbps
            case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                return "EHRPD"; // ~ 1-2 Mbps
            case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                return "EV-DO Revision B"; // ~ 5 Mbps
            case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                return "HSPAP"; // ~ 10-20 Mbps
            case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                return "IDEN"; // ~25 kbps
            case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                return "LTE"; // ~ 10+ Mbps
            // Unknown
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
            default:
                return "Unknown";
        }
    }

    private static int getLevel() {
        int level;
        Class c;
        try {
            c = Class.forName("android.telephony.SignalStrength");
        } catch (Exception e) { return SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
        Method m;
        boolean isGsm;
        try {
            m = c.getMethod("isGsm", (Class[])null);
            isGsm = (boolean)m.invoke(signalStrength);
        } catch (Exception e) { return SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
        if (isGsm) {
            try {
                m = c.getMethod("getLteLevel", (Class[])null);
                level = (int)m.invoke(signalStrength);
            } catch (Exception e) { level = SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
            if (level == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                try {
                    m = c.getMethod("getTdScdmaLevel", (Class[])null);
                    level = (int)m.invoke(signalStrength);
                } catch (Exception e) { level = SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
                if (level == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                    try {
                        m = c.getMethod("getGsmLevel", (Class[])null);
                        level = (int)m.invoke(signalStrength);
                    } catch (Exception e) { level = SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
                }
            }
        } else {
            int cdmaLevel;
            int evdoLevel;
            try {
                m = c.getMethod("getCdmaLevel", (Class[])null);
                cdmaLevel = (int)m.invoke(signalStrength);
            } catch (Exception e) { cdmaLevel = SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
            try {
                m = c.getMethod("getEvdoLevel", (Class[])null);
                evdoLevel = (int)m.invoke(signalStrength);
            } catch (Exception e) { evdoLevel = SIGNAL_STRENGTH_NONE_OR_UNKNOWN; }
            if (evdoLevel == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                /* We don't know evdo, use cdma */
                level = cdmaLevel;
            } else if (cdmaLevel == SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
                /* We don't know cdma, use evdo */
                level = evdoLevel;
            } else {
                /* We know both, use the lowest level */
                level = cdmaLevel < evdoLevel ? cdmaLevel : evdoLevel;
            }
        }
        return level;
    }
}
