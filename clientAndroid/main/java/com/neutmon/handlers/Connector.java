package com.neutmon.handlers;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Connector {
    public Socket connSock;

    public Connector() {
        connSock = new Socket();
    }

    public void connect(String addr, int port) throws IOException {
            connSock.connect(new InetSocketAddress(addr, port));
    }

    public void closeConnection() throws IOException {
        connSock.close();
    }
}
