package com.neutmon.handlers;

import com.google.gson.Gson;
import com.neutmon.Constants;
import com.neutmon.exceptions.ControllerException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.zip.Deflater;

public class Controller {
    private Socket ctrlSock;
    private static final String TAG = "Controller";

    public Controller(Socket sock) { ctrlSock = sock; }

    public <T> void sendCtrlMsg(int msg, Dictionary<String, T> extra) throws ControllerException {
        if (msg < Constants.CONTROLLER_OK_MSG ||
                msg > Constants.CONTROLLER_CLIENT_TEST_INIT_ERROR) {
            throw new ControllerException("Message is not valid");
        }
        try {
            sendMsgOnTCPSocket(msg, extra);
        } catch (IOException e) {
            throw new ControllerException("Controller socket error on sending message");
        }
    }

    public void sendCtrlMsg(int msg) throws ControllerException {
        sendCtrlMsg(msg, null);
    }

    private <T> void sendMsgOnTCPSocket(int msg, Dictionary<String, T> extra) throws IOException {
        OutputStream outStream = ctrlSock.getOutputStream();
        if (extra == null) {
            outStream.write(ByteBuffer.allocate(4).putInt(4).array());
            outStream.write(ByteBuffer.allocate(4).putInt(msg).array());
        } else {
            Gson gson = new Gson();
            String jsonFromDict = gson.toJson(extra);
            //Compress data to send to the controller
            Deflater deflater = new Deflater();
            byte[] jsonBytes = jsonFromDict.getBytes("UTF-8");
            deflater.setInput(jsonBytes);
            deflater.finish();
            byte[] compressedBytes = new byte[jsonBytes.length + 100];
            int len = deflater.deflate(compressedBytes);
            deflater.end();
            //Send compressed data only if they are smaller than the uncompressed ones
            /*
            if (len < jsonBytes.length) {
                Log.d(TAG, "Send Compressed");
                outStream.write(ByteBuffer.allocate(4).putInt(4 + len).array());
                outStream.write(ByteBuffer.allocate(4).putInt(msg).array());
                outStream.write(compressedBytes, 0, len);
            } else {
                Log.d(TAG, "Send NOT Compressed");
                outStream.write(ByteBuffer.allocate(4).putInt(4 + jsonBytes.length).array());
                outStream.write(ByteBuffer.allocate(4).putInt(msg).array());
                outStream.write(jsonBytes);
            }
            */
            outStream.write(ByteBuffer.allocate(4).putInt(4 + jsonBytes.length).array());
            outStream.write(ByteBuffer.allocate(4).putInt(msg).array());
            outStream.write(jsonBytes);
            System.out.println(jsonFromDict);
        }
    }

    public byte[] recvCtrlMsg() throws ControllerException {
        byte[] receivedMsg;
        try {
            receivedMsg = recvMsgOnTCPSocket();
        } catch (IOException e) {
            if (e instanceof SocketTimeoutException) {
                throw new ControllerException(
                        "Controller socket timeout on receiving message, " + e.getMessage()
                );
            } else {
                throw new ControllerException(
                        "Controller socket error on receiving message, " + e.getMessage()
                );
            }
        }
        //The first 4 bytes are the operation
        int operation = ByteBuffer.wrap(Arrays.copyOfRange(receivedMsg, 0, 4)).getInt();
        if (operation < Constants.CONTROLLER_START_UB_MSG ||
                operation > Constants.CONTROLLER_CLIENT_TEST_INIT_ERROR) {
            throw new ControllerException("Message is not valid");
        }
        if (operation >= Constants.CONTROLLER_START_UB_MSG &&
                operation <= Constants.CONTROLLER_START_DC_MSG) {
            if (receivedMsg.length < 8) {
                throw new ControllerException("Received message doesn't contain port");
            }
            int extra = ByteBuffer.wrap(Arrays.copyOfRange(receivedMsg, 4, 8)).getInt();
            if (extra != Constants.BT_PORT &&
                    !(extra >= Constants.BT_LOW_PORT &&
                            extra <= Constants.BT_HIGH_PORT)) {
                throw new ControllerException(
                        "The specified port for a start measure message is not valid"
                );
            }
        }
        return receivedMsg;
    }

    //The message structure is the following
    //|<--4 bytes-->|<--------length bytes-------->|
    //|   length    | op(4 bytes) | port(optional) |
    private byte[] recvMsgOnTCPSocket() throws IOException, ControllerException {
        ByteArrayOutputStream res = new ByteArrayOutputStream();
        InputStream inStream = ctrlSock.getInputStream();

        //Get the length of the message
        byte[] len = receiveBytes(inStream, 4);
        int length = ByteBuffer.wrap(len).getInt();

        //Get the operation
        byte[] op = receiveBytes(inStream, 4);
        res.write(op);
        //char operation = (char)op[3];

        //Subtract the operation length to the length of the total message
        length -= 4;
        //If some bytes have still to be retrieved (the server is sending the port or the stats)
        if (length != 0) {
            byte[] toReceive = receiveBytes(inStream, length);
            if (length < 13) {
                //The port is a String and must be converted in int
                int port = Integer.parseInt(new String(toReceive));
                res.write(ByteBuffer.allocate(4).putInt(port).array());
            } else {
                res.write(ByteBuffer.allocate(4).putFloat(
                        ByteBuffer.wrap(toReceive, 0, 4).getFloat()
                ).array());
                res.write(ByteBuffer.allocate(4).putFloat(
                        ByteBuffer.wrap(toReceive, 4, 4).getFloat()
                ).array());
                res.write(ByteBuffer.allocate(4).putInt(
                        ByteBuffer.wrap(toReceive, 8, 4).getInt()
                ).array());
                res.write(toReceive, 12, 1);
            }
        }
        return res.toByteArray();
    }



    private byte[] receiveBytes(InputStream inStream, int toReceive)
            throws IOException, ControllerException {
        int receivedBytes = 0;
        byte[] inBytes = ByteBuffer.allocate(toReceive).array();
        while (toReceive != 0) {
            receivedBytes = inStream.read(inBytes, inBytes.length - toReceive, toReceive);
            if (receivedBytes < 0) {
                throw new ControllerException("Failed to receive data");
            }
            toReceive -= receivedBytes;
        }
        return inBytes;
    }
}
