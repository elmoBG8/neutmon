package com.neutmon.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.neutmon.utilities.CheckServerAddress;
import com.neutmon.R;

import com.neutmon.Constants;

public class MessageReceiver extends BroadcastReceiver {
    private Context context;

    public MessageReceiver(Context ctx) {
        context = ctx;
    }

    @Override
    public void onReceive(Context ctx, Intent intent) {
        Activity a = (Activity)context;
        if (intent.getAction() == Constants.BROADCAST_ACTION_END_TEST) {
            //Reset the data maintained by StatReceiver to draw the ResultView during the test
            StatReceiver.reset();
            //Switch the button text from "Stop" to "Test"
            ((Button)a.findViewById(R.id.startButton)).setText(R.string.start_test);
            //Check if the server address and port are correct (The button becomes grey if one of
            //them is not valid)
            new CheckServerAddress().execute(
                    ((EditText)a.findViewById(R.id.etServerAddr)).getText().toString(),
                    ((EditText)a.findViewById(R.id.etServerPort)).getText().toString(),
                    a.findViewById(R.id.ivServerAddr),
                    a.findViewById(R.id.ivServerPort),
                    a.findViewById(R.id.startButton)
            );
        } else if (intent.getAction() == Constants.BROADCAST_ACTION_MSG_FROM_TEST) {
            //Write at most 60 lines in the messages TextView
            String message = intent.getStringExtra(Constants.INTENT_MESSAGE);
            TextView textView = a.findViewById(R.id.messages);
            String[] lines = textView.getText().toString().split("\n");
            String result = "";
            for (int i = lines.length - 59 <= 0 ? 0 : lines.length - 59; i < lines.length; i++) {
                if (lines[i] != "") {
                    result = result + lines[i] + "\n";
                }
            }
            textView.setText(result + message);
        }
    }
}
