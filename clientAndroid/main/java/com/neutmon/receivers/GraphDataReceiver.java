package com.neutmon.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;

import com.neutmon.Constants;
import com.neutmon.R;
import com.neutmon.views.GraphView;

import java.util.ArrayList;

public class GraphDataReceiver extends BroadcastReceiver {
    private Context context;
    private ArrayList<Double> sigStrengthList;
    private ArrayList<Double> sigStrengthInstantsList;
    private static final String bittorrentTraffic = "Bittorrent traffic ";
    private static final String controlTraffic = "Control traffic ";
    private static final String uplinkSpeed = "Uplink speed";
    private static final String downlinkSpeed = "Downlink speed";

    public GraphDataReceiver(Context ctx) {
        context = ctx;
        sigStrengthList = new ArrayList<>();
        sigStrengthInstantsList = new ArrayList<>();
    }

    private class SignalArrays {
        public double[] signalStrengths;
        public double[] sInstants;

        public SignalArrays(double[] ss, double[] si) {
            signalStrengths = ss;
            sInstants = si;
        }
    }

    @Override
    public void onReceive(Context ctx, Intent intent) {
        Activity a = (Activity)context;
        double[] throughputs = null;
        double[] tInstants = null;
        double tReference = 0.0;
        double signalStrength = -1;
        double sInstant = 0.0;
        if (intent.getAction() == Constants.BROADCAST_ACTION_SIGNAL_STRENGTH) {
            signalStrength = intent.getIntExtra(Constants.INTENT_SIGNAL_STRENGTH, -1);
            sInstant = intent.getDoubleExtra(Constants.INTENT_SIGNAL_STRENGTH_INSTANT, 0.0);
            //Invalid values
            if (signalStrength == -1 || sInstant == 0.0) {
                return;
            }
            sigStrengthList.add(signalStrength);
            sigStrengthInstantsList.add(sInstant);
            //Remove too old values
            double start;
            while (sigStrengthList.size() > 1) {
                //Remove the head only if the second value is too old too
                start = sigStrengthInstantsList.get(1);
                if (sInstant - start > Constants.DEFAULT_TEST_DURATION * 3) {
                    sigStrengthList.remove(0);
                    sigStrengthInstantsList.remove(0);
                } else {
                    break;
                }
            }
            return;
        } else if (intent.getAction() == Constants.BROADCAST_ACTION_THROUGPUT) {
            throughputs = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUTS_ARRAY);
            tInstants = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY);
            tReference = intent.getDoubleExtra(Constants.INTENT_THROUGHPUT_INSTANT_REFERENCE, 0.0);
            String phase = intent.getStringExtra(Constants.INTENT_THROUGHPUT_PHASE);

            int graphId;
            String graphTitle;
            if (phase.equals(Constants.UPLINK_BITTORRENT_PHASE)) {
                graphTitle = bittorrentTraffic + uplinkSpeed;
                graphId = R.id.graph_ub;
            } else if (phase.equals(Constants.UPLINK_CONTROL_PHASE)) {
                graphTitle = controlTraffic + uplinkSpeed;
                graphId = R.id.graph_uc;
            } else if (phase.equals(Constants.DOWNLINK_BITTORRENT_PHASE)) {
                graphTitle = bittorrentTraffic + downlinkSpeed;
                graphId = R.id.graph_db;
            } else { //phase.equals(Constants.DOWNLINK_CONTROL_PHASE)
                graphTitle = controlTraffic + downlinkSpeed;
                graphId = R.id.graph_dc;
            }
            View graph = a.findViewById(graphId);
            ViewGroup parent = (ViewGroup)graph.getParent();
            int graphIndex = parent.indexOfChild(graph);
            parent.removeViewAt(graphIndex);

            SignalArrays signalArrays = getSignalArrays(tReference, tInstants);

            //Adjust throughput values
            int magnitude = normalizeValues(throughputs);
            String mag = "";
            if (magnitude == 1) {
                mag = "·10";
            } else if (magnitude != 0) {
                mag = "·10" + toSubscript(magnitude);
            }
            if(mag.length() > 4) {
                System.out.println("too long");
            }
            parent.addView(
                    new GraphView(
                        context, graphId, tInstants, throughputs,
                        signalArrays.sInstants, signalArrays.signalStrengths,
                        graphTitle, "Time (s)", "Speed (B" + mag +"/s)",
                        "Signal strength", Constants.DEFAULT_TEST_DURATION
                    ),
                    graphIndex
            );
        }
    }

    private int normalizeValues(double[] values) {
        int magnitude = 1;
        double max = getMax(values);
        if (max != 0.0) {
            magnitude = (int)Math.floor(Math.log10(max));
        }
        double scale = Math.pow(10, magnitude);
        for (int i = 0; i < values.length; i++) {
            values[i] /= scale;
        }
        return magnitude;
    }

    private double getMax(double[] values) {
        double largest = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < values.length; i++) {
            if (values[i] < 0.0) {
                values[i] = 0.0;
            }
            if (values[i] > largest) {
                largest = values[i];
            }
        }
        return largest;
    }

    private <T> String toSubscript(T value) {
        String string = value.toString();
        try {
            Double.parseDouble(string);
        } catch (Exception e) {
            return "";
        }
        String result = "";
        for (int i = 0; i < string.length(); i++) {
            result += getSubscript(string.charAt(i));
        }
        return result;
    }

    private String getSubscript(char c) {
        switch (c) {
            case '0':case '4':case '5':case '6':case '7':case '8':case '9':
                return String.valueOf((char)(c - '0' + '⁰'));
            case '1':
                return "¹";
            case '2':
                return "²";
            case '3':
                return "³";
            case '-':
                return "⁻";
            case '+':
                return "⁺";
            default:
                return "";
        }
    }

    private SignalArrays getSignalArrays(double start, double[] tInstants) {
        double[] signalStrenghs, sInstants;
        double maxTInstant = start + tInstants[tInstants.length - 1];
        ArrayList<Double> tmpI = new ArrayList<>();
        ArrayList<Double> tmpS = new ArrayList<>();
        int ssilSize = sigStrengthInstantsList.size();
        //Exclude too old and too new values
        for (int i = 0; i < ssilSize; i++) {
            if (i+1 < ssilSize && sigStrengthInstantsList.get(i+1) - start < 0) {
                continue;
            }
            if (sigStrengthInstantsList.get(i) > maxTInstant) {
                break;
            }
            tmpI.add((sigStrengthInstantsList.get(i) - start));
            tmpS.add(sigStrengthList.get(i));
        }
        //The first value point drawn in the graph has x = 0
        if (tmpI.size() > 0) {
            tmpI.set(0, 0d);
        }
        signalStrenghs = new double[tmpS.size()];
        sInstants = new double[tmpI.size()];
        for (int i = 0; i < tmpI.size(); i++) {
            signalStrenghs[i] = tmpS.get(i);
            sInstants[i] = tmpI.get(i);
        }
        return new SignalArrays(signalStrenghs, sInstants);
    }
}
