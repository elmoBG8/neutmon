package com.neutmon;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.neutmon.utilities.ArraySaver;
import com.neutmon.utilities.TestResult;

import java.io.*;

public class MainActivity extends AppCompatActivity {

    private EditText etServerAddr;
    private EditText etServerPort;
    private TextView serverAddr;
    private TextView serverPort;
    private ProgressBar progressBar;
    private ProgressBar progressCircle;
    private StatusReceiver statusReceiver;
    private TextView percentage;
    //aggiunto io
    private TextView stateServer;
    private boolean started = false;
    private boolean youCanStop = false;
    private Button buttonStart;
    private Intent clientServiceIntent;
    //
    private boolean testIsRunning = false;
    private boolean resultAvailable = false;
    private String timestamp;
    private double[] throughput_bt = null;
    private double[] instant_bt = null;
    private double mean_bt_th = 0.0;
    private double[] throughput_ct = null;
    private double[] instant_ct = null;
    private double mean_ct_th = 0.0;
    private double[] throughput_ht = null;
    private double[] instant_ht = null;
    private double mean_ht_th = 0.0;
    private ImageView tickImage;
    private static final String TAG = "StartActivity";
    private ArraySaver arraySaver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.starter_layout);
        etServerAddr = findViewById(R.id.etServerAddr);
        etServerPort = findViewById(R.id.etServerPort);
        serverAddr = findViewById(R.id.textView2);
        serverPort = findViewById(R.id.textView4);
        progressBar = findViewById(R.id.progressBar);
        progressCircle = findViewById(R.id.progressCircle);
        percentage = findViewById(R.id.percentage);
        //aggiunto io
        stateServer = findViewById(R.id.stateServer);
        buttonStart = findViewById(R.id.testButton);
        //
        tickImage = findViewById(R.id.tickImage);
        statusReceiver = new StatusReceiver();
        arraySaver = new ArraySaver(getApplicationContext());
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        resultAvailable = oldResultExist();
        getLastResult();
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_BT_DOWNLINK_PASSED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_CT_DOWNLINK_PASSED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_HT_DOWNLINK_PASSED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_BT_UPLINK_PASSED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_CT_UPLINK_PASSED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_HT_UPLINK_PASSED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_TEST_COMPLETED));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_INIT_TEST));
        lbm.registerReceiver(statusReceiver, new IntentFilter(Constants.INTENT_ALL_SERVER_BUSY));

    }

    private void getLastResult(){
        TestResult ts;
        String f;
        File[] list;
        if(resultAvailable){
            try {
                list = arraySaver.getAllTestResultName();
                f = list[list.length-1].getName();
                ts = arraySaver.loadTestResult(f);
                throughput_bt = ts.getThBt();
                throughput_ct = ts.getThCt();
                throughput_ht = ts.getThHt();
                mean_bt_th = ts.getmThBtAsArray()[0];
                mean_ct_th = ts.getmThCtAsArray()[0];
                mean_ht_th = ts.getmThHtAsArray()[0];
                timestamp = f;
            } catch (IOException e) {
                Log.e(TAG, "IOException while loading last test result");
            }
        }
    }

    private boolean oldResultExist() {
        File[] list = arraySaver.getAllTestResultName();
        return list != null && list.length > 0;
    }

    public void startStopService(View view){
        if(testIsRunning&&youCanStop){
            //Toast.makeText(this, "Test is currently running", Toast.LENGTH_LONG).show();
            stopService(clientServiceIntent);
            progressBar.setVisibility(View.INVISIBLE);
            percentage.setVisibility(View.INVISIBLE);
            progressCircle.setVisibility(View.INVISIBLE);
            started = false;
            youCanStop = false;
            buttonStart.setText("START");
            Log.d(TAG,"Test stopped by utent");
        }
        else {
            if (testIsRunning && !youCanStop) {
                Toast.makeText(this, "Test is going: requires some minutes", Toast.LENGTH_LONG).show();

            }
            if (!started) {//qua parte il test, ma non posso aprire socket in main activity
                started = true;
                Toast.makeText(this, "Test is going: requires some minutes", Toast.LENGTH_LONG).show();
                if (etServerPort != null && etServerAddr != null) {
                    String serverAddr = etServerAddr.getText().toString();
                    String serverPort = etServerPort.getText().toString();
                    if (checkFields(serverAddr, serverPort)) {
                        clientServiceIntent = new Intent(this, TestService.class);
                        clientServiceIntent.putExtra(Constants.SERVER_ADDRESS, serverAddr);
                        clientServiceIntent.putExtra(Constants.SERVER_PORT, serverPort);
                        Log.d(TAG, "StartService");
                        startService(clientServiceIntent);
                        youCanStop = true;
                        buttonStart.setText("STOP");
                        //
                    } else {
                        Log.e(TAG, "IP and/or port not valid");
                    }
                } else {
                    Log.e(TAG, "Null editTexts");
                }
            }
        }
    }

    public void makeInvisibleSettings(){
        etServerAddr.setVisibility(View.INVISIBLE);
        etServerPort.setVisibility(View.INVISIBLE);
        serverPort.setVisibility(View.INVISIBLE);
        serverAddr.setVisibility(View.INVISIBLE);
    }

    public void makeVisibleSettings(){
        etServerPort.setVisibility(View.VISIBLE);
        etServerAddr.setVisibility(View.VISIBLE);
        serverAddr.setVisibility(View.VISIBLE);
        serverPort.setVisibility(View.VISIBLE);
    }

    private boolean checkFields(String serverAddr, String serverPort){
        boolean valid = true;
        String[] piecesIP = serverAddr.split("\\.");
        if (piecesIP.length != 4)
            valid = false;
        for (String p : piecesIP) {
            if (Integer.parseInt(p) > 255 || Integer.parseInt(p) < 0)
                valid = false;
        }
        if(Integer.parseInt(serverPort) <0 || Integer.parseInt(serverPort)>65535)
            valid = false;
        return valid;
    }

    public void displayResults(View view){
        if(!resultAvailable){
            Toast.makeText(this, "Result NOT Available", Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(this, "Result Available", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, GraphActivity.class)
                        .putExtra(Constants.EXTRA_ARRAY_BT_TH, throughput_bt)
                        .putExtra(Constants.EXTRA_ARRAY_CT_TH, throughput_ct)
                        .putExtra(Constants.EXTRA_ARRAY_HT_TH, throughput_ht)
                        .putExtra(Constants.EXTRA_MEAN_TH_BT, mean_bt_th)
                        .putExtra(Constants.EXTRA_MEAN_TH_CT, mean_ct_th)
                        .putExtra(Constants.EXTRA_MEAN_TH_HT, mean_ht_th)
                        .putExtra(Constants.EXTRA_TIMESTAMP, timestamp));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusReceiver);
    }

    public class StatusReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            //aggiunto io
            stateServer.setVisibility(View.INVISIBLE);
            //
            TestResult testResult;
            String action = intent.getAction();
            if(action == null)
                return;
            switch (action){

                //aggiunto io
                case Constants.INTENT_ALL_SERVER_BUSY:
                    Log.d(TAG,"All server busy");
                    stateServer.setVisibility(View.VISIBLE);
                    stateServer.setText( "Every server is busy. Try again later");//sfrutto la percentuale della barra dei progressi...
                case Constants.INTENT_TEST_FAILED:
                    Log.d(TAG,"Test failed");
                    stateServer.setVisibility(View.VISIBLE);
                    stateServer.setText( "Sorry, test failed. Try again");//sfrutto la percentuale della barra dei progressi...
                    testIsRunning = false;
                    started = false;
                    buttonStart.setText("START");
                    //
                case Constants.INTENT_BT_DOWNLINK_PASSED:
                    throughput_bt = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUTS_ARRAY);
                    instant_bt = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY);
                    mean_bt_th = intent.getDoubleExtra(Constants.EXTRA_MEAN_TH, 0.0);
                    addProgress();
                    break;
                case Constants.INTENT_CT_DOWNLINK_PASSED:
                    throughput_ct = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUTS_ARRAY);
                    instant_ct = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY);
                    mean_ct_th = intent.getDoubleExtra(Constants.EXTRA_MEAN_TH, 0.0);
                    addProgress();
                    break;
                case Constants.INTENT_HT_DOWNLINK_PASSED:
                    throughput_ht = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUTS_ARRAY);
                    instant_ht = intent.getDoubleArrayExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY);
                    mean_ht_th = intent.getDoubleExtra(Constants.EXTRA_MEAN_TH, 0.0);
                    addProgress();
                    break;
                case Constants.INTENT_BT_UPLINK_PASSED:
                    addProgress();
                    break;
                case Constants.INTENT_CT_UPLINK_PASSED:
                    addProgress();
                    break;
                case Constants.INTENT_HT_UPLINK_PASSED:
                    addProgress();
                    break;
                case Constants.INTENT_TEST_COMPLETED:
                    progressCircle.setVisibility(View.INVISIBLE);
                    tickImage.setVisibility(View.VISIBLE);
                    makeVisibleSettings();
                    testIsRunning = false;
                    started = false;
                    buttonStart.setText("START");
                    if(throughput_bt != null && throughput_ct != null && throughput_ht != null
                            && instant_bt != null && instant_ct != null && instant_ht != null) {
                        resultAvailable = true;
                        testResult = new TestResult();
                        testResult.setThBt(throughput_bt);
                        testResult.setThCt(throughput_ct);
                        testResult.setThHt(throughput_ht);
                        testResult.setmThBt(mean_bt_th);
                        testResult.setmThCt(mean_ct_th);
                        testResult.setmThHt(mean_ht_th);
                        try {
                            timestamp = arraySaver.saveTestResult(testResult);
                            if(timestamp != null) {
                                Log.d(TAG, "Saved");
                            }
                        } catch (IOException e) {
                            Log.d(TAG, "Error while saving");
                        }
                    }
                    break;
                case Constants.INTENT_INIT_TEST:
                    testIsRunning = true;
                    tickImage.setVisibility(View.INVISIBLE);
                    resultAvailable = false;
                    percentage.setText("0 %");
                    progressBar.setProgress(0);
                    makeInvisibleSettings();
                    progressBar.setVisibility(View.VISIBLE);
                    percentage.setVisibility(View.VISIBLE);
                    progressCircle.setVisibility(View.VISIBLE);
                    break;
                default:
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void addProgress(){
        progressBar.incrementProgressBy(1);
        percentage.setText(String.valueOf((int)(16.67*progressBar.getProgress())) + "%");
    }

}
