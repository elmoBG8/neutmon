package com.neutmon.exceptions;

public class TesterException extends Exception {
    public int errorCode;

    public TesterException(int errCode, String msg) {
        super(msg);
        errorCode = errCode;
    }
}
