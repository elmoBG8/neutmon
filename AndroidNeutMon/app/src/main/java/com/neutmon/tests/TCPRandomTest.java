package com.neutmon.tests;

import com.neutmon.Constants;
import com.neutmon.exceptions.TesterException;

import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Random;

import static java.sql.DriverManager.println;

public class TCPRandomTest extends TCPTest {

    private byte[] randomBytesResponse;
    private byte[] randomBytesRequest;
    private int offsetResponse;
    private int offsetRequest;

    public TCPRandomTest(int transferDim) {
        super(transferDim);
        randomBytesResponse = new byte[transferDim * 1000];
        new Random().nextBytes(randomBytesResponse);
        offsetResponse = 0;
        randomBytesRequest = new byte[Constants.BITTORRENT_REQUEST_TOTAL_LENGTH * Constants.NUMBER_OF_REQUESTS * 100];
        new Random().nextBytes(randomBytesRequest);
        offsetRequest = 0;
    }

    private byte[] buildRequest() {
        byte[] req = Arrays.copyOfRange(
                randomBytesRequest,
                offsetRequest,
                offsetRequest + (Constants.BITTORRENT_REQUEST_TOTAL_LENGTH * Constants.NUMBER_OF_REQUESTS)
        );
        offsetRequest += (Constants.BITTORRENT_REQUEST_TOTAL_LENGTH * Constants.NUMBER_OF_REQUESTS);
        if (offsetRequest >= randomBytesRequest.length) {
            offsetRequest = 0;
        }
        return req;
    }

    private byte[] buildResponse() {
        byte[] res = Arrays.copyOfRange(
                randomBytesResponse,
                offsetResponse,
                offsetResponse + transferDim * Constants.NUMBER_OF_REQUESTS
        );
        offsetResponse += transferDim * Constants.NUMBER_OF_REQUESTS;
        if (offsetResponse >= randomBytesResponse.length) {
            offsetResponse = 0;
        }
        return res;
    }

    private void uplinkPreparation(Socket sendSocket) throws TesterException {
        receiveFromSocket(sendSocket, 68);
        byte[] handshakeSend = generateRandomBytes(68);
        sendOnSocket(sendSocket, handshakeSend);
        byte[] unchoke = generateRandomBytes(5);
        sendOnSocket(sendSocket, unchoke);
        receiveFromSocket(sendSocket, 5);
    }

    private void downlinkPreparation(Socket recvSocket) throws  TesterException {
        byte[] handshakeSend = generateRandomBytes(68);
        sendOnSocket(recvSocket, handshakeSend);
        //Receive handshake
        receiveFromSocket(recvSocket, 68);
        //Receive unchoke
        receiveFromSocket(recvSocket, 5);
        byte[] interest = generateRandomBytes(5);
        sendOnSocket(recvSocket, interest);
    }

    @Override
    public void uplinkTest(Socket sendSocket, int duration) throws Exception{
        uplinkPreparation(sendSocket);
        long stop, start;
        stop = start = System.currentTimeMillis() / 1000L;
        //TODO manage time
        //uploadedDataRegister.activate();
        if (duration == 0)
            duration = Constants.DEFAULT_TEST_DURATION;
        while (stop - start < duration) {
            receiveFromSocket(sendSocket,
                    Constants.BITTORRENT_REQUEST_TOTAL_LENGTH * Constants.NUMBER_OF_REQUESTS);
            byte[] resp = buildResponse();
            sendOnSocket(sendSocket, resp);
            //TODO manage graph
            //displayGraph(uploadedDataRegister, Constants.UPLINK_CONTROL_PHASE);
            stop = System.currentTimeMillis() / 1000L;
        }
        //uploadedDataRegister.deactivate();
        byte[] choke = generateRandomBytes(5);
        sendOnSocket(sendSocket, choke);
    }

    @Override
    public void downlinkTest(Socket recvSocket, Dictionary<String, Integer> intervals) throws TesterException{
        downlinkPreparation(recvSocket);
        long start = System.currentTimeMillis();
        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(3);
        df.setGroupingUsed(false);
        intervals.put(df.format((double)start / 1000d).replace(',', '.'), 0);
        //downloadedDataRegister.activate();
        //send request (80 pieces of 0x4000 bytes) and receive response
        //if choke received (5 bytes), stop test
        while(true) {
            byte[] req = buildRequest();
            sendOnSocket(recvSocket, req);
            byte[] recBytes = receiveFromSocket(
                    recvSocket, transferDim * Constants.NUMBER_OF_REQUESTS, intervals
            );
            if (recBytes.length == 5) {
                println("Downlink Control test ended");
                break;
            }
            //displayGraph(downloadedDataRegister, Constants.DOWNLINK_CONTROL_PHASE);
        }
        //downloadedDataRegister.deactivate();
        long stop = System.currentTimeMillis() - 5000L;
        double interval = (double)(stop - start) / 1000d;
    }
}
