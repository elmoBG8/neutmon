package com.neutmon.tests;


import com.neutmon.exceptions.TesterException;
import com.neutmon.utilities.DataRegister;

import java.net.Socket;
import java.util.Dictionary;

public abstract class Test {

    int transferDim;
    public DataRegister uploadedDataRegister;
    public DataRegister downloadedDataRegister;

    Test(int transferDim){
        this.transferDim = transferDim;
        this.uploadedDataRegister = new DataRegister();
        this.downloadedDataRegister = new DataRegister();
    }

    public abstract void sendOnSocket(Socket sendSocket, byte[] data) throws TesterException;
    public abstract byte[] receiveFromSocket(Socket receiveSocket, int length) throws TesterException;
    public abstract byte[] receiveFromSocket(
            Socket receiveSocket, int length, Dictionary<String, Integer> intervals) throws TesterException;
    public abstract void uplinkTest(Socket sendSocket, int duration) throws Exception;
    public abstract void downlinkTest(Socket receiveSocket, Dictionary<String, Integer> intervals) throws Exception;
}
