package com.neutmon;

public class Constants {
    public static final String SERVER_ADDRESS = "SERVER_ADDR";
    public static final String SERVER_PORT = "SERVER_PORT";
    public static final String TEST_DATE = "test_date";
    public static final String CONNECTION_OPERATOR = "connection_operator";
    public static final String CONNECTION_TECHNOLOGY = "connection_technology";

    public static final int BT_PORT = 6881;
    public static final int BT_LOW_PORT = 50000;
    public static final int BT_HIGH_PORT = 65536;
    public static final String TEST_RESULT_DIR = "data";
    public static final int CONTROLLER_START_UB_MSG = 0;
    public static final int CONTROLLER_START_UC_MSG = 1;
    public static final int CONTROLLER_START_UH_MSG = 2;
    public static final int CONTROLLER_START_DB_MSG = 3;
    public static final int CONTROLLER_START_DC_MSG = 4;
    public static final int CONTROLLER_START_DH_MSG = 5;
    public static final int CONTROLLER_SEND_META_DATA_MSG = 6;
    public static final int CONTROLLER_ABORT_MEASURE_MSG = 7;
    public static final int CONTROLLER_FINISH_MEASURE_MSG = 8;
    public static final int CONTROLLER_OK_MSG = 9;
    public static final int CONTROLLER_CLIENT_CONNECT_REFUSED_ERROR = 10;  //Connection refused or reset or abort timeout on selected port
    public static final int CONTROLLER_CLIENT_CONNECT_TIMEOUT_ERROR = 11;  //Connection timeout on selected port
    public static final int CONTROLLER_CLIENT_CONNECT_GENERIC_ERROR = 12;  //Connection generic error
    public static final int CONTROLLER_CLIENT_TEST_RESET_ERROR = 13;  //Connection reset when testing
    public static final int CONTROLLER_CLIENT_TEST_ABORT_ERROR = 14;  //Connection aborted when testing
    public static final int CONTROLLER_CLIENT_TEST_TIMEOUT_ERROR = 15;  //Connection timeout when testing
    public static final int CONTROLLER_CLIENT_TEST_GENERIC_ERROR = 16;  //Generic error when testing
    public static final int CONTROLLER_CLIENT_TEST_INIT_ERROR = 17;  //Generic error when initialising tester

    public static final int TESTER_OK = 9;
    public static final int TESTER_CONNECT_REFUSED_ERROR = 10;
    public static final int TESTER_CONNECT_TIMEOUT_ERROR = 9;
    public static final int TESTER_CONNECT_GENERIC_ERROR = 12;
    public static final int TESTER_TEST_RESET_ERROR = 13;
    public static final int TESTER_TEST_ABORT_ERROR = 14;
    public static final int TESTER_TEST_GENERIC_ERROR = 16;
    public static final int TESTER_INIT_CLIENT_ERROR = 17;
    public static final int TESTER_INIT_SERVER_ERROR = 18;
    public static final int TESTER_ACCEPT_GENERIC_ERROR = 20;

    public static final int TEST_UPLINK_PHASE = 0;
    public static final int TEST_DOWNLINK_PHASE = 1;

    public static final int TEST_SPEEDTEST_TYPE = 0;
    public static final int TEST_TRACEROUTE_TYPE = 1;

    public static final int DEFAULT_TEST_DURATION = 10;  //seconds
    public static final int DEFAULT_TRANSFER_DIMENSION = 1024 * 1024;  //Bytes
    public static final int DEFAULT_BT_TRANSFER_DIMENSION = 16397;  //Bytes
    public static final int DEFAULT_HTTP_TRANSFER_DIMENSION = 260 + 9437184 + 2; //Bytes
    public static final int BITTORRENT_PORT = 6881;
    public static final int BITTORRENT_ALTERNATIVE_PORT = 51413;
    public static final int BITTORRENT_REQUEST_LENGTH = 13;
    public static final int BITTORRENT_REQUEST_TOTAL_LENGTH = 17;
    public static final int NUMBER_OF_REQUESTS = 80;
    public static final byte BITTORRENT_REQUEST_TYPE = 0x6;
    public static final int BITTORRENT_BLOCK_DIMENSION = 0x4000;
    public static final int BITTORRENT_PIECE_DIMENSION = 0x20000;
    public static final int BITTORRENT_START_INDEX = 0x0;
    public static final int BITTORRENT_START_OFFSET = 0x0;
    public static final int BITTORRENT_RESPONSE_LENGTH = 0x9;
    public static final byte BITTORRENT_PIECE_TYPE = 0x7;

    public static final String BROADCAST_ACTION_SEND_STATS = "com.neutmon.SEND_STATS";
    public static final String BROADCAST_ACTION_MSG_FROM_TEST = "com.neutmon.TEST_MSG";
    public static final String BROADCAST_ACTION_END_TEST = "com.neutmon.END_TEST";
    public static final String BROADCAST_ACTION_NETWORK_INFO = "com.neutmon.NETWORK_INFO";
    public static final String BROADCAST_ACTION_THROUGPUT = "com.neutmon.THROUGHPUT";
    public static final String BROADCAST_ACTION_SIGNAL_STRENGTH = "com.neutmon.SIGNAL_STRENGTH";

    public static final String INTENT_MESSAGE = "INTENT_MESSAGE";

    public static final String STAT_THROUGHPUT = "STAT_THROUGHPUT";
    public static final String STAT_INTERVAL = "STAT_INTERVAL";
    public static final String STAT_PHASE = "STAT_PHASE";
    public static final String STAT_TEST_INDEX = "STAT_INDEX";
    public static final String STAT_POPULATION_COUNT = "STAT_POPULATION_COUNT";
    public static final String STAT_STD_DEVIATION = "STAT_STD_DEVIATION";

    public static final String SAVED_RESULT_VIEW_ELEMENTS = "SAVED_RESULT_VIEW_ELEMENTS";
    public static final String SAVED_RESULT_MESSAGES = "SAVED_RESULT_MESSAGES";
    public static final String SAVED_RESULT_NET_INFO = "NETWORK_INFO";

    public static final String INTENT_THROUGHPUTS_ARRAY = "INTENT_THROUGHPUTS";
    public static final String INTENT_THROUGHPUT_INSTANTS_ARRAY = "INTENT_THROUGHPUT_INSTANTS";
    public static final String INTENT_THROUGHPUT_INSTANT_REFERENCE = "INTENT_THROUGHPUT_INSTANT_REFERENCE";
    public static final String INTENT_SIGNAL_STRENGTH = "INTENT_SIGNAL_STRENGTH";
    public static final String INTENT_SIGNAL_STRENGTH_INSTANT = "INTENT_SIGNAL_STRENGTH_INSTANT";
    public static final String INTENT_THROUGHPUT_PHASE = "INTENT_THROUGHPUT_PHASE";

    public static final String INTENT_BT_UPLINK_PASSED = "INTENT_BT_UPLINK_PASSED";
    public static final String INTENT_CT_UPLINK_PASSED = "INTENT_CT_UPLINK_PASSED";
    public static final String INTENT_HT_UPLINK_PASSED = "INTENT_HT_UPLINK_PASSED";
    public static final String INTENT_BT_DOWNLINK_PASSED = "INTENT_BT_DOWNLINK_PASSED";
    public static final String INTENT_CT_DOWNLINK_PASSED = "INTENT_CT_DOWNLINK_PASSED";
    public static final String INTENT_HT_DOWNLINK_PASSED = "INTENT_HT_DOWNLINK_PASSED";
    public static final String INTENT_TEST_COMPLETED = "INTENT_TEST_COMPLETED";
    public static final String INTENT_INIT_TEST = "INTENT_INIT_TEST";
    public static final String INTENT_ALL_SERVER_BUSY = "INTENT_ALL_SERVER_BUSY"; //Aggiunto io - tutti i server occupati
    public static final String INTENT_TEST_FAILED = "INTENT_TEST_FAILED"; //Aggiunto io - tutti i server occupati


    public static final String EXTRA_ARRAY_BT_TH = "EXTRA_ARRAY_BT_TH";
    public static final String EXTRA_ARRAY_CT_TH = "EXTRA_ARRAY_CT_TH";
    public static final String EXTRA_ARRAY_HT_TH = "EXTRA_ARRAY_HT_TH";
    public static final String EXTRA_MEAN_TH_BT = "EXTRA_MEAN_TH_BT";
    public static final String EXTRA_MEAN_TH_CT = "EXTRA_MEAN_TH_CT";
    public static final String EXTRA_MEAN_TH_HT = "EXTRA_MEAN_TH_HT";
    public static final String EXTRA_LOAD_FILENAME = "EXTRA_LOAD_FILENAME";
    public static final String EXTRA_ARRAY_BT_TIME = "EXTRA_ARRAY_BT_TIME";
    public static final String EXTRA_ARRAY_CT_TIME = "EXTRA_ARRAY_CT_TIME";
    public static final String EXTRA_ARRAY_HT_TIME = "EXTRA_ARRAY_HT_TIME";

    public static final String UPLINK_BITTORRENT_PHASE = "ub";
    public static final String UPLINK_CONTROL_PHASE = "uc";
    public static final String DOWNLINK_BITTORRENT_PHASE = "db";
    public static final String DOWNLINK_CONTROL_PHASE = "dc";
    public static final int TEST_RESULT_SELECTED = 1;
    public static final String EXTRA_TIMESTAMP = "EXTRA_TIMESTAMP";
    public static final String EXTRA_MEAN_TH = "EXTRA_MEAN_TH";
    public static final String FILE_STORAGE_NAME = "th_storage.dat";

    //aggiunti io
    static final int SERVER_TROVATO = 1;
    static final int FINE_OPERAZIONI = 2;
    static final int TUTTI_OCCUPATI = 3;
}
