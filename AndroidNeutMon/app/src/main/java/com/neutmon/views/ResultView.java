package com.neutmon.views;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neutmon.R;

import org.apache.commons.math3.distribution.TDistribution;

import java.text.DecimalFormat;

public class ResultView extends RelativeLayout {
    public double[] speedValues;
    public String netInfo;
    private static final int TOTAL_BAR_LENGTH = 300;
    private static final int BAR_HEIGHT = 20;

    public ResultView(Context context,
                      double u_bt, double u_bt_i, double u_bt_stdDev, int u_bt_count,
                      double u_ct, double u_ct_i, double u_ct_stdDev, int u_ct_count,
                      double d_bt, double d_bt_i, double d_bt_stdDev, int d_bt_count,
                      double d_ct, double d_ct_i, double d_ct_stdDev, int d_ct_count,
                      String info) {
        super(context);

        speedValues = new double[] {u_bt, u_bt_i, u_bt_stdDev, u_bt_count,
                       u_ct, u_ct_i, u_ct_stdDev, u_ct_count,
                       d_bt, d_bt_i, d_bt_stdDev, d_bt_count,
                       d_ct, d_ct_i, d_ct_stdDev, d_ct_count};
        netInfo = info;
        setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        //setOrientation(LinearLayout.VERTICAL);
        setId(R.id.result);
        inflate(getContext(), R.layout.result_box, this);
        //Write the network info
        ((TextView)findViewById(R.id.network_info)).setText(netInfo);
        //Find the maximum value ?_?? + ?_??_i in order to set the proper scale
        double max = getMaxValue();
        //Use this scale to draw the bars in order to have the largest bar as long as TOTAL_BAR_LENGTH
        double scale = (max == 0d ? 0d : TOTAL_BAR_LENGTH / max);

        int[] ids = {R.id.u_bt_bar, R.id.u_bt_bar_1, R.id.u_bt_bar_2,
                R.id.u_ct_bar, R.id.u_ct_bar_1, R.id.u_ct_bar_2,
                R.id.d_bt_bar, R.id.d_bt_bar_1, R.id.d_bt_bar_2,
                R.id.d_ct_bar, R.id.d_ct_bar_1, R.id.d_ct_bar_2,
                R.id.u_bt_text, R.id.u_ct_text, R.id.d_bt_text, R.id.d_ct_text};

        int oneDip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
        for (int i = 0; i < 4; i++) {
            double throughput = speedValues[i * 4];
            double leftInterval = speedValues[i * 4 + 1];
            double rightInterval = leftInterval;
            //Set the dimensions of the bar showing the throughput
            findViewById(ids[i*3]).setLayoutParams(
                    new LinearLayout.LayoutParams(
                            leftInterval > throughput ? 0 :
                                    (int)((throughput - leftInterval) * scale),
                            BAR_HEIGHT
                    )
            );
            //Set the dimensions of the bar showing the left confidence interval
            findViewById(ids[i*3 + 1]).setLayoutParams(
                    //Add +oneDip as the style of the left interval has android:left="-1dip"
                    new LinearLayout.LayoutParams(
                            leftInterval <= 0d ? 0 : (int)(
                                    (leftInterval > throughput ?
                                            throughput :
                                            leftInterval
                                    ) * scale) + oneDip,
                            BAR_HEIGHT
                    )
            );
            //Set the dimensions of the bar showing the right confidence interval
            findViewById(ids[i*3 + 2]).setLayoutParams(
                    new LinearLayout.LayoutParams(
                            throughput <= 0d ? 0 :
                                    rightInterval <= 0d ? oneDip : (int)Math.max(rightInterval * scale, oneDip),
                            BAR_HEIGHT
                    )
            );
            //Add the text containing the numerical value of mean and CI
            ((TextView)findViewById(ids[12 + i])).setText(getText(speedValues[i*4], speedValues[i*4 + 1]));
        }
        //Upload two-sample t-test
        if (u_bt_count > 1 && u_ct_count > 1) {
            double tmp1 = (u_bt_stdDev * u_bt_stdDev) / u_bt_count;
            double tmp2 = (u_ct_stdDev * u_ct_stdDev) / u_ct_count;
            double tmp3 = tmp1 + tmp2;
            double se = Math.sqrt(tmp3);
            double degFreedom = tmp3 * tmp3 /
                    (tmp1 * tmp1 / (u_bt_count - 1) + tmp2 * tmp2 / (u_ct_count - 1));
            double t = (u_bt - u_ct) / se;
            if (t > 0) {
                t *= -1d;
            }
            TDistribution tDistribution = new TDistribution(degFreedom);
            double p = tDistribution.cumulativeProbability(t);
            if (2 * p < 0.01) { //Reject the null hypothesis
                if (u_bt < u_ct) {
                    ((TextView)findViewById(R.id.u_bt_text)).setTextColor(Color.MAGENTA);
                } else {
                    ((TextView)findViewById(R.id.u_ct_text)).setTextColor(Color.MAGENTA);
                }
            }
        }
        //Download two-sample t-test
        if (d_bt_count > 1 && d_ct_count > 1) {
            double tmp1 = (d_bt_stdDev * d_bt_stdDev) / d_bt_count;
            double tmp2 = (d_ct_stdDev * d_ct_stdDev) / d_ct_count;
            double tmp3 = tmp1 + tmp2;
            double se = Math.sqrt(tmp3);
            double degFreedom = tmp3 * tmp3 /
                    (tmp1 * tmp1 / (d_bt_count - 1) + tmp2 * tmp2 / (d_ct_count - 1));
            double t = (d_bt - d_ct) / se;
            if (t > 0) {
                t *= -1d;
            }
            TDistribution tDistribution = new TDistribution(degFreedom);
            double p = tDistribution.cumulativeProbability(t);
            if (2 * p < 0.01) { //Reject the null hypothesis
                if (d_bt < d_ct) {
                    ((TextView)findViewById(R.id.d_bt_text)).setTextColor(Color.MAGENTA);
                } else {
                    ((TextView)findViewById(R.id.d_ct_text)).setTextColor(Color.MAGENTA);
                }
            }
        }
    }

    private double getMaxValue() {
        double max = -1f;
        for(int i = 0; i < 4; i++) {
            double value = speedValues[i*4] + speedValues[i*4 + 1];
            if (value > max) {
                max = value;
            }
        }
        return max;
    }

    private String getText(double throughput, double interval) {
        int magnitude = getMagnitude(throughput);
        if (magnitude != 0) {
            throughput = throughput / (pow(1024f, magnitude));
        }
        String magnitudeT = getMagnitudeString(magnitude);
        magnitude = getMagnitude(interval);
        if (magnitude != 0) {
            interval = interval / (pow(1024f, magnitude));
        }
        String magnitudeI = getMagnitudeString(magnitude);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return df.format(throughput) + " " + magnitudeT +
                (interval <= 0f? "" : " ± " + df.format(interval) + " " + magnitudeI);

    }

    private int getMagnitude(double value) {
        if (value < 0) {
            value = -value;
        }
        int magnitude = 0;
        while (value / 1024f > 1f || magnitude > 2) {
            value = value / 1024f;
            magnitude++;
        }
        return magnitude;
    }

    private String getMagnitudeString(int magnitude) {
        switch (magnitude) {
            case 0:
                return "B";
            case 1:
                return "KB";
            case 2:
                return "MB";
            default:
                return "GB";
        }
    }

    private double pow(double value, int exponent) {
        if (exponent <= 0) {
            return 1d;
        }
        double result = value;
        for (int i = 1; i < exponent; i++) {
            result *= value;
        }
        return result;
    }
}