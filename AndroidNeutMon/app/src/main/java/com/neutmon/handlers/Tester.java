package com.neutmon.handlers;

import com.neutmon.Constants;
import com.neutmon.exceptions.TesterException;
import com.neutmon.tests.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Dictionary;

public class Tester {
    private int port;
    private Socket testSocket;

    public Tester(int port) throws TesterException {
        this.port = port;
        testSocket = new Socket();
        try {
            testSocket.setReuseAddress(true);
        } catch (SocketException e) {
            throw new TesterException(
                    Constants.TESTER_INIT_CLIENT_ERROR,
                    "Unable to create socket for tests"
            );
        }
    }

    public void connect (String address) throws TesterException {
        SocketAddress sockAddr = new InetSocketAddress(address, port);
        try {
            testSocket.connect(sockAddr);
        } catch (IOException e) {
            throw new TesterException(
                        Constants.TESTER_CONNECT_GENERIC_ERROR,
                        "Unable to connect to server on port " + port + " address " + address
            );
        }
    }

    public void doTest(Test test, int phase, Dictionary<String, Integer> result)
            throws TesterException {
        try {
            if (phase == Constants.TEST_UPLINK_PHASE) {
                test.uplinkTest(testSocket, 0);
            } else if (phase == Constants.TEST_DOWNLINK_PHASE) {
                test.downlinkTest(testSocket, result);
            }
        } catch (Exception e) {
             if (e instanceof  TesterException) {
                throw (TesterException)e;
            } else {
                throw new TesterException(
                        Constants.TESTER_TEST_GENERIC_ERROR,
                        "Test failed due to socket error, " + e.getMessage()
                );
            }
        }
    }

    public void closeTestConnection() {
        try {
            testSocket.shutdownInput();
            testSocket.shutdownOutput();
            testSocket.close();
        } catch (IOException e) {
            System.err.println("Error on shutdown, " + e.getMessage());
        }
    }

    public void finishTest() {
        try {
            testSocket.close();
        } catch (IOException e) {
            System.err.println("Error while closing socket, " + e.getMessage());
        }
    }
}
