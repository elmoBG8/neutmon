package com.neutmon.utilities;

import android.content.Context;
import android.util.Log;


import com.neutmon.Constants;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.LinkedList;

public class ArraySaver {

    private File dir;
    private static final String TAG = "ArraySaver";
    private static final String BT_DOUBLE_FILE = "btDouble.dat";
    private static final String CT_DOUBLE_FILE = "ctDouble.dat";
    private static final String HT_DOUBLE_FILE = "htDouble.dat";
    private static final String BT_MEAN_FILE = "btMean.dat";
    private static final String CT_MEAN_FILE = "ctMean.dat";
    private static final String HT_MEAN_FILE = "htMean.dat";
    private static final String BT_TIME_FILE = "btTime.dat";
    private static final String CT_TIME_FILE = "ctTime.dat";
    private static final String HT_TIME_FILE = "htTime.dat";

    public ArraySaver(Context context) {
        dir = new File(context.getFilesDir(), Constants.TEST_RESULT_DIR);
        if(!dir.isDirectory())
            if(dir.mkdir()){
                Log.d(TAG, "Dir Created");
            }else{
                Log.d(TAG, "Dir not Created");
            }
    }

    public String saveTestResult(TestResult ts) throws IOException {
        String timestamp = new Timestamp(System.currentTimeMillis()).toString();
        File currentTestDir = new File(dir, timestamp);
        if(currentTestDir.mkdir()) {
            saveArray(currentTestDir, BT_DOUBLE_FILE, ts.getThBt());
            saveArray(currentTestDir, CT_DOUBLE_FILE, ts.getThCt());
            saveArray(currentTestDir, HT_DOUBLE_FILE, ts.getThHt());
            saveArray(currentTestDir, BT_MEAN_FILE, ts.getmThBtAsArray());
            saveArray(currentTestDir, CT_MEAN_FILE, ts.getmThCtAsArray());
            saveArray(currentTestDir, HT_MEAN_FILE, ts.getmThHtAsArray());
            return timestamp;
        }else{
            Log.e(TAG, "Test Result directory error");
            return null;
        }
    }

    public TestResult loadTestResult(String fileName) throws IOException {
        File file = new File(dir, fileName);
        TestResult ts = new TestResult();
        if (!file.isDirectory()){
            Log.e(TAG, "Test Result file does not exist");
            return null;
        }else{
            File[] listFile = file.listFiles();
            for(File f : listFile) {
                switch (f.getName()) {
                    case BT_DOUBLE_FILE:
                        ts.setThBt(loadArray(file, f.getName()));
                        break;
                    case CT_DOUBLE_FILE:
                        ts.setThCt(loadArray(file, f.getName()));
                        break;
                    case HT_DOUBLE_FILE:
                        ts.setThHt(loadArray(file, f.getName()));
                        break;
                    case BT_TIME_FILE:
                        ts.setTimeBt(loadArray(file, f.getName()));
                        break;
                    case CT_TIME_FILE:
                        ts.setTimeCt(loadArray(file, f.getName()));
                        break;
                    case HT_TIME_FILE:
                        ts.setTimeHt(loadArray(file, f.getName()));
                        break;
                    case BT_MEAN_FILE:
                        ts.setmThBt(loadArray(file, f.getName())[0]);
                        Log.d(TAG, "BT MEAN LOADED");
                        break;
                    case CT_MEAN_FILE:
                        ts.setmThCt(loadArray(file, f.getName())[0]);
                        Log.d(TAG, "CT MEAN LOADED");
                        break;
                    case HT_MEAN_FILE:
                        ts.setmThHt(loadArray(file, f.getName())[0]);
                        Log.d(TAG, "HT MEAN LOADED");
                        break;
                }
            }
            return ts;
        }
    }

    private double[] loadArray(File dir, String fileName) throws IOException {
        File file = new File(dir, fileName);
        LinkedList<Double> loadedDouble = new LinkedList<>();
        DataInputStream dataInputStreamStream = new DataInputStream(new FileInputStream(file));
        try {
            double v;
            while (dataInputStreamStream.available() > 0) {
                v = dataInputStreamStream.readDouble();
                loadedDouble.add(v);
            }
        }catch (IOException e){
            Log.d(TAG, "Error loading array from file");
        }
        double[] array = new double[loadedDouble.size()];
        for(int i =0; i<loadedDouble.size(); i++)
            array[i] = loadedDouble.get(i);
        return array;
    }

    private void saveArray(File dir, String fileName, double[] array) throws FileNotFoundException {
        File file = new File(dir, fileName);
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
        for (double v : array) {
            try {
                dataOutputStream.writeDouble(v);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public File[] getAllTestResultName(){
        return dir.listFiles();
    }

}
