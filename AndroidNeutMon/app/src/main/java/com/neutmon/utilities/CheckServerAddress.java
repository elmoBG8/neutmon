package com.neutmon.utilities;

import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.ImageView;

import com.neutmon.R;

import org.apache.commons.validator.routines.InetAddressValidator;

public class CheckServerAddress extends AsyncTask {
    private ImageView ivServerAddr;
    private ImageView ivServerPort;
    private Button startButton;
    private boolean portOK;
    private boolean serverOK;

    @Override
    protected Object doInBackground(Object[] params) {
        String serverAddr = (String)params[0];
        String serverPort = (String)params[1];
        ivServerAddr = (ImageView)params[2];
        ivServerPort = (ImageView)params[3];
        startButton = (Button)params[4];
        serverOK = checkAddress(serverAddr);
        portOK = checkPort(serverPort);
        return serverOK && portOK;
    }

    @Override
    protected void onPostExecute(Object result) {
        if((boolean)result) {
            startButton.setTextColor(Color.rgb(33, 33, 33));
        } else {
            startButton.setTextColor(Color.rgb(189, 189, 189));
        }
        if (serverOK) {
            ivServerAddr.setImageResource(R.drawable.tick);
        } else {
            ivServerAddr.setImageResource(R.drawable.cross);
        }
        if (portOK) {
            ivServerPort.setImageResource(R.drawable.tick);
        } else {
            ivServerPort.setImageResource(R.drawable.cross);
        }

    }

    public static boolean checkAddress(String address) {
        return InetAddressValidator.getInstance().isValid(address);
    }
    public static boolean checkPort(String port) {
        try {
            int p = Integer.parseInt(port);
            if (p < 1 || p > 65535) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
