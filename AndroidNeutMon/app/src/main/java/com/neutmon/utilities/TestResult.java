package com.neutmon.utilities;


public class TestResult {

    private double[] thBt = null;
    private double[] thCt = null;
    private double[] thHt = null;

    private double mThBt = 0.0;
    private double mThCt = 0.0;
    private double mThHt = 0.0;

    private double[] timeBt = null;
    private double[] timeCt = null;
    private double[] timeHt = null;

    public void setmThBt(double mThBt) {
        this.mThBt = mThBt;
    }

    public void setmThCt(double mThCt) {
        this.mThCt = mThCt;
    }

    public void setmThHt(double mThHt) {
        this.mThHt = mThHt;
    }

    public double[] getmThBtAsArray() {
        double[] meanArrayBt = new double[1];
        meanArrayBt[0] =  mThBt;
        return meanArrayBt;
    }

    public double[] getmThCtAsArray() {
        double[] meanArrayCt = new double[1];
        meanArrayCt[0] =  mThCt;
        return meanArrayCt;
    }

    public double[] getmThHtAsArray() {
        double[] meanArrayHt = new double[1];
        meanArrayHt[0] =  mThHt;
        return meanArrayHt;
    }

    public void setThBt(double[] thBt) {
        this.thBt = thBt;
    }

    public void setTimeBt(double[] timeBt) {
        this.timeBt = timeBt;
    }

    public void setThCt(double[] thCt) {
        this.thCt = thCt;
    }

    public void setTimeCt(double[] timeCt) {
        this.timeCt = timeCt;
    }

    public void setThHt(double[] thHt) {
        this.thHt = thHt;
    }

    public void setTimeHt(double[] timeHt) {
        this.timeHt = timeHt;
    }

    public double[] getThBt() {
        return thBt;
    }


    public double[] getThCt() {
        return thCt;
    }

    public double[] getThHt() {
        return thHt;
    }

    public double[] getTimeBt() {
        return timeBt;
    }

    public double[] getTimeCt() {
        return timeCt;
    }

    public double[] getTimeHt() {
        return timeHt;
    }
}
