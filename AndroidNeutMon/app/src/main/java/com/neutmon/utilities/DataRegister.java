package com.neutmon.utilities;

import org.apache.commons.math3.distribution.TDistribution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class DataRegister {
    private static double DEFAULT_WINDOW_SIZE = 300;
    private class Entry {
        public double start;
        public double end;
        public int amount;

        public Entry(double s, double e, int a) {
            start = s;
            end = e;
            amount = a;
        }
    }

    private boolean active;
    private ArrayList<Double> throughputs;
    private ArrayList<Double> timeInstants;

    private ArrayList<Entry> list;

    public DataRegister() {
        list = new ArrayList<>();
    }

    public void activate() {
        active = true;
    }

    public void deactivate() {
        active = false;
    }

    public void addData(double start, double end, int amount) {
        list.add(new Entry(start, end, amount));
    }

    public void clearRegister() {
        if (list != null) {
            list.clear();
        }
        if (throughputs != null) {
            throughputs.clear();
        }
        if (timeInstants != null) {
            timeInstants.clear();
        }
        deactivate();
    }

    private void computeThroughputs(double windowSize) {//QUA VIENE DECISO COME OTTENERE GLI ARRAY DEI THROUGHPUT E DEGLI ISTANTI
        throughputs = new ArrayList<>();
        timeInstants = new ArrayList<>();
        Iterator<Entry> iterator = list.iterator();
        double timeInterval = 0.0;
        double throughput;
        int amount = 0;
        Entry e = null;
        double reference = getReferenceInstant();
        while(iterator.hasNext()) {
            e = iterator.next();
            double interval = e.end - e.start;
            timeInterval += interval;
            amount += e.amount;
            if (timeInterval >= windowSize) {
                throughput = amount / timeInterval * 1000;
                if (throughput != Double.NaN) {
                    throughputs.add(throughput);
                    timeInstants.add(e.end / 1000.0 - reference);
                    amount = 0;
                    timeInterval = 0.0;
                }
            }
        }
    }

    public double getThroughputMean(double windowSize) {
        computeThroughputs(windowSize);
        if (throughputs.size() == 0) {
            return 0.0;
        }
        double amount = 0.0;
        Iterator<Double> iterator = throughputs.iterator();
        while (iterator.hasNext()) {
            amount += iterator.next();
        }
        return amount / throughputs.size();
    }

    public double getThroughputStandardDeviation(double mean) {
        if (throughputs.size() == 0) {
            return 0.0;
        }
        double amount = 0.0;
        Iterator<Double> iterator = throughputs.iterator();
        while (iterator.hasNext()) {
            double difference = iterator.next() - mean;
            amount += difference * difference;
        }
        amount /= throughputs.size() - 1;
        return Math.sqrt(amount);
    }

    public double getThroughputMean() {
        return getThroughputMean(DEFAULT_WINDOW_SIZE);
    }

    public double getConfidenceInterval(double probability, double mean) {
        if (throughputs.size() < 2) {
            return -1.0;
        }
        TDistribution tDistribution = new TDistribution(throughputs.size() - 1);
        double value = tDistribution.inverseCumulativeProbability(1 - ((1 - probability) / 2));
        return value * getThroughputStandardDeviation(mean) / Math.sqrt(throughputs.size());
    }

    public HashMap<Double, Integer> getIntervals() {
        HashMap<Double, Integer> hashMap = new HashMap<>();
        Iterator<Entry> iterator = list.iterator();
        Entry entry;
        double timeAmount;
        if (iterator.hasNext()) {
            entry = iterator.next();
            hashMap.put(entry.start, 0);
            hashMap.put(entry.end, entry.amount);
            timeAmount = entry.end;
            while (iterator.hasNext()) {
                entry = iterator.next();
                timeAmount += entry.end - entry.start;
                hashMap.put(timeAmount, entry.amount);
            }
        }
        return hashMap;
    }

    public double[] getTimeInstants() {
        if (timeInstants.size() != throughputs.size()) {
            computeThroughputs(DEFAULT_WINDOW_SIZE);
        }
        int size = timeInstants.size();
        double[] instants = new double[size];
        for (int i = 0; i < size; i++) {
            instants[i] = timeInstants.get(i);
        }
        return instants;
    }

    public double[] getThroughputs(int smooth) {
        computeThroughputs(DEFAULT_WINDOW_SIZE);
        int half = smooth / 2;
        smooth = half * 2 + 1;
        double value;
        int size = throughputs.size();
        double[] tr = new double[size];
        for (int i = 0; i < size; i++) {
            value = 0.0;
            for (int j = i - half; j <= i + half; j++) {
                if (j < 0) continue;
                if (j >= size) break;
                value += throughputs.get(j);
            }
            value /= smooth;
            tr[i] = value;
        }
        return tr;
    }

    public double[] getThroughputs() {
        return getThroughputs(1);
    }

    public double getReferenceInstant() {
        if (list == null || list.size() == 0) return 0.0;
        return list.get(0).start / 1000d;
    }

    public int getSampleCount() {
        return throughputs.size();
    }

    public boolean isActive() {
        return active;
    }
}
