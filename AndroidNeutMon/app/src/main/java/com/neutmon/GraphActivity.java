package com.neutmon;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.neutmon.utilities.ArraySaver;
import com.neutmon.utilities.TestResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class GraphActivity extends AppCompatActivity {

    private GraphView graphView;
    private ArraySaver arraySaver;
    private TextView timestampTextView;
    private TextView btTextView;
    private TextView ctTextView;
    private TextView htTextView;
    private String filename;
    private double[] throughput_bt = null;
    private double[] throughput_ct = null;
    private double[] throughput_ht = null;
    private double mThBt = 0.0;
    private double mThCt = 0.0;
    private double mThHt = 0.0;
    private static final String TAG = "ArraySaver";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_layout);
        graphView = findViewById(R.id.graph);
        timestampTextView = findViewById(R.id.timestampTextView);
        btTextView = findViewById(R.id.btTextView);
        ctTextView = findViewById(R.id.ctTextView);
        htTextView = findViewById(R.id.htTextView);
        arraySaver = new ArraySaver(getApplicationContext());
        getExtra();
        displayData();
    }

    private void getExtra() {
        Intent intent = getIntent();
        throughput_bt = intent.getDoubleArrayExtra(Constants.EXTRA_ARRAY_BT_TH);
        throughput_ct = intent.getDoubleArrayExtra(Constants.EXTRA_ARRAY_CT_TH);
        throughput_ht = intent.getDoubleArrayExtra(Constants.EXTRA_ARRAY_HT_TH);
        mThBt = mRound(intent.getDoubleExtra(Constants.EXTRA_MEAN_TH_BT, 0.0));
        mThCt = mRound(intent.getDoubleExtra(Constants.EXTRA_MEAN_TH_CT, 0.0));
        mThHt = mRound(intent.getDoubleExtra(Constants.EXTRA_MEAN_TH_HT, 0.0));
        filename = intent.getStringExtra(Constants.EXTRA_TIMESTAMP);
    }

    private double mRound(double v){
        return (Math.round((v/1000000.0) * 100.0) / 100.0)*8;
    }

    protected void displayData() {
        double mega = 1000000;
        double bit = 8;
        Arrays.sort(throughput_bt);
        Arrays.sort(throughput_ct);
        Arrays.sort(throughput_ht);
        DataPoint[] throughputBtDPArray = new DataPoint[throughput_bt.length];
        DataPoint[] throughputCtDPArray = new DataPoint[throughput_ct.length];
        DataPoint[] throughputHtDPArray = new DataPoint[throughput_ht.length];
        int i;
        for (i = 0; i < throughput_bt.length; i++) {
            throughputBtDPArray[i] = new DataPoint((throughput_bt[i] / mega) * bit, ((double) i / (double) (throughput_bt.length - 1)));
        }
        for (i = 0; i < throughput_ct.length; i++) {
            throughputCtDPArray[i] = new DataPoint((throughput_ct[i] / mega) * bit, ((double) i / (double) (throughput_ct.length - 1)));
        }
        for (i = 0; i < throughput_ht.length; i++) {
            throughputHtDPArray[i] = new DataPoint((throughput_ht[i] / mega) * bit, ((double) i / (double) (throughput_ht.length - 1)));
        }
        LineGraphSeries<DataPoint> btSeries = new LineGraphSeries<>(throughputBtDPArray);
        LineGraphSeries<DataPoint> ctSeries = new LineGraphSeries<>(throughputCtDPArray);
        LineGraphSeries<DataPoint> htSeries = new LineGraphSeries<>(throughputHtDPArray);

        timestampTextView.setText(filename);
        btTextView.setText(String.valueOf(mThBt));
        ctTextView.setText(String.valueOf(mThCt));
        htTextView.setText(String.valueOf(mThHt));
        btSeries.setTitle("BT Throughput");
        btSeries.setColor(Color.YELLOW);
        ctSeries.setTitle("CT Throughput");
        htSeries.setColor(Color.RED);
        htSeries.setTitle("HT Throughput");
        btSeries.setColor(Color.BLUE);
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(40);

        // set manual Y bounds
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMinY(0);
        graphView.getViewport().setMaxY(1.1);
        graphView.getLegendRenderer().setVisible(true);
        graphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM);
        graphView.getViewport().setScalable(true);
        graphView.addSeries(btSeries);
        graphView.addSeries(ctSeries);
        graphView.addSeries(htSeries);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_graph, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ArrayList<String> fileNames = new ArrayList<>();
        switch (item.getItemId()) {
            case R.id.load_id:
                File[] listFiles = arraySaver.getAllTestResultName();
                for(File f : listFiles)
                    fileNames.add(f.getName());
                Intent loadIntent = new Intent(this, ListActivity.class);
                loadIntent.putExtra(Constants.EXTRA_LOAD_FILENAME, fileNames);
                startActivityForResult(loadIntent, Constants.TEST_RESULT_SELECTED);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TestResult testResult;
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.TEST_RESULT_SELECTED:
                    filename = data.getStringExtra(Constants.EXTRA_LOAD_FILENAME);
                    try {
                        testResult = arraySaver.loadTestResult(filename);
                        throughput_bt = testResult.getThBt();
                        throughput_ct = testResult.getThCt();
                        throughput_ht = testResult.getThHt();
                        mThBt = mRound(testResult.getmThBtAsArray()[0]);
                        mThCt = mRound(testResult.getmThCtAsArray()[0]);
                        mThHt = mRound(testResult.getmThHtAsArray()[0]);
                        graphView.removeAllSeries();
                        displayData();
                    } catch (IOException e) {
                        Log.e(TAG, "Error while loading");
                    }
                    break;
            }
        }
    }
}
