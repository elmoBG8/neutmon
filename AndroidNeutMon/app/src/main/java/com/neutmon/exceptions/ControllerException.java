package com.neutmon.exceptions;

/**
 * Created by lorenzo on 24/10/17.
 */

public class ControllerException extends Exception {
    public ControllerException(String msg) {
        super(msg);
    }
}
