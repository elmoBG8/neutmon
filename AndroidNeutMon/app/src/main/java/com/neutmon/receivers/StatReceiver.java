package com.neutmon.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.neutmon.Constants;
import com.neutmon.MainActivity;
import com.neutmon.R;
import com.neutmon.utilities.Connectivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StatReceiver extends BroadcastReceiver {
    private static double u_bt = 0d, u_bt_i = 0d, u_ct = 0d, u_ct_i = 0d, d_bt = 0d, d_bt_i = 0d, d_ct = 0d, d_ct_i = 0d;
    private static double u_bt_stdDev, u_ct_stdDev, d_bt_stdDev, d_ct_stdDev;
    private static int u_bt_count = -1, u_ct_count = -1, d_bt_count = -1, d_ct_count = -1;
    private MainActivity context;
    private static int networkSubtype;
    private static Date date;
    private static String operator;
    private static String netInfo = "";

    public StatReceiver(Context c) {
        context = (MainActivity)c;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == Constants.BROADCAST_ACTION_SEND_STATS) {
            double throughput = intent.getDoubleExtra(Constants.STAT_THROUGHPUT, 0d);
            double stdDev = intent.getDoubleExtra(Constants.STAT_STD_DEVIATION, 0d);
            int count = intent.getIntExtra(Constants.STAT_POPULATION_COUNT, 0);
            double interval = intent.getDoubleExtra(Constants.STAT_INTERVAL, 0d);
            int phase = intent.getIntExtra(Constants.STAT_PHASE, -1);
            char index = intent.getCharExtra(Constants.STAT_TEST_INDEX, 'n');
            if (phase == -1 || index == 'n') {
                return;
            }
            if (phase == Constants.TEST_UPLINK_PHASE) {
                if (index == 'b') {
                    u_bt = throughput;
                    u_bt_i = interval;
                    u_bt_stdDev = stdDev;
                    u_bt_count = count;
                } else {
                    u_ct = throughput;
                    u_ct_i = interval;
                    u_ct_stdDev = stdDev;
                    u_ct_count = count;
                }
            } else {
                if (index == 'b') {
                    d_bt = throughput;
                    d_bt_i = interval;
                    d_bt_stdDev = stdDev;
                    d_bt_count = count;
                } else {
                    d_ct = throughput;
                    d_ct_i = interval;
                    d_ct_stdDev = stdDev;
                    d_ct_count = count;
                }
            }
            RelativeLayout resultView = this.context.findViewById(R.id.result);
            ViewGroup parent = (ViewGroup)resultView.getParent();
            int resultIndex = parent.indexOfChild(resultView);
            parent.removeViewAt(resultIndex);
            /*
            this.context.resultView = new ResultView(this.context,
                    u_bt, u_bt_i, u_bt_stdDev, u_bt_count,
                    u_ct, u_ct_i, u_ct_stdDev, u_ct_count,
                    d_bt, d_bt_i, d_bt_stdDev, d_bt_count,
                    d_ct, d_ct_i, d_ct_stdDev, d_ct_count,
                    netInfo);
            parent.addView(this.context.resultView, resultIndex);
            */
        } else if (intent.getAction() == Constants.BROADCAST_ACTION_NETWORK_INFO) {
            operator = Connectivity.getNetworkOperator(this.context);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            date = new Date();
            netInfo = "";
            netInfo += dateFormat.format(date) + '\n';
            netInfo += "Connected by " + operator;
            if (Connectivity.isConnectedMobile(this.context)) {
                networkSubtype = Connectivity.getConnectionType(this.context);
                netInfo += "\nTechnology: " + Connectivity.getConnectionSubtypeName(networkSubtype);
            } else {
                networkSubtype = -1;
            }
        }

    }
    public static void reset() {
        u_bt = u_bt_i = u_ct = u_ct_i = d_bt = d_bt_i = d_ct = d_ct_i = 0f;
        u_bt_stdDev = u_ct_stdDev = d_bt_stdDev = d_ct_stdDev = 0d;
        u_bt_count = u_ct_count = d_bt_count = d_ct_count = -1;
        netInfo = "";
        date = null;
        operator = "";
        networkSubtype = -1;
    }
}
