package com.neutmon;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.neutmon.exceptions.ControllerException;
import com.neutmon.exceptions.TesterException;
import com.neutmon.handlers.Connector;
import com.neutmon.handlers.Controller;
import com.neutmon.handlers.Tester;
import com.neutmon.tests.TCPBTTest;
import com.neutmon.tests.TCPHTTPTest;
import com.neutmon.tests.TCPRandomTest;
import com.neutmon.tests.Test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.TimeZone;

public class TestService extends IntentService {

    private Connector connector;
    private Test btTest, ctTest, htTest, currentTest;
    private Tester tester;
    private String serverAddr;
    private int serverPort;
    private static final String TAG = "TestService";
    private Dictionary<String, String> metaData;
    private Dictionary<String, Integer> result;
    //aggiunto io

    private Socket sock;
    private DataOutputStream dout;
    private DataInputStream din;
    private String indirizzoIPServer, portaServer;
    private boolean stoppato;
    private boolean fallito;
    //

    //aggiunto io per DB


    private String timestamp;
    private double[] throughput_bt = null;
    private double[] instant_bt = null;
    private double mean_bt_th = 0.0;
    private double[] throughput_ct = null;
    private double[] instant_ct = null;
    private double mean_ct_th = 0.0;
    private double[] throughput_ht = null;
    private double[] instant_ht = null;
    private double mean_ht_th = 0.0;
    //

    public TestService(){
        super("TestService");
    }

    private void initComponents(@Nullable Intent intent){
        assert intent != null;
        connector = new Connector();
        btTest = new TCPBTTest(Constants.DEFAULT_BT_TRANSFER_DIMENSION);
        ctTest = new TCPRandomTest(Constants.DEFAULT_BT_TRANSFER_DIMENSION);
        htTest = new TCPHTTPTest(Constants.DEFAULT_HTTP_TRANSFER_DIMENSION);
        serverAddr = intent.getStringExtra(Constants.SERVER_ADDRESS);
        serverPort = Integer.parseInt(intent.getStringExtra(Constants.SERVER_PORT));
        currentTest = null;
        metaData = null;
        result = null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int phase =-1, port = -1, operation;
        String action;
        byte[] receivedCommand;
        initComponents(intent);

        //aggiunto io: qua cerco server libero
        try{
            stoppato = false;
            fallito = false;
            sock = new Socket(serverAddr,serverPort);//ci deve essere ip e porta del Load Balancer
            dout = new DataOutputStream(sock.getOutputStream());
            din = new DataInputStream(sock.getInputStream());
        }
        catch(Exception e){
            Log.e(TAG, e.getMessage());
        }
        if(!inviaRichiestaServer()) {//se la richiesta è fallita termino
            LocalBroadcastManager.getInstance(this).sendBroadcast(
                    new Intent(Constants.INTENT_ALL_SERVER_BUSY)
            );
            Log.d(TAG,"Richiesta fallita");
            return;
        }
        //
        try {
            connector.connect(indirizzoIPServer, Integer.parseInt(portaServer)); //modificato io
        }catch (IOException e){
            fallito = true;
            LocalBroadcastManager.getInstance(this).sendBroadcast(
                    new Intent(Constants.INTENT_TEST_FAILED));
            Log.e(TAG, "Unable to connect using connector object");
            //Toast.makeText(getApplicationContext(), "Unable to connect", Toast.LENGTH_LONG).show();
            return;
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(
                new Intent(Constants.INTENT_INIT_TEST)
        );
        Controller controller = new Controller(connector.connSock);
        while(!stoppato&&!fallito){
            try {
                receivedCommand = controller.recvCtrlMsg();
            }catch (ControllerException e){
                Log.e(TAG, "Controller Exception: "+ e.getMessage());
                return;
            }
            operation = ByteBuffer.wrap(receivedCommand, 0, 4).getInt();
            switch (operation) {
                case Constants.CONTROLLER_ABORT_MEASURE_MSG:
                    Log.d(TAG, "CONTROLLER_ABORT_MEASURE_MSG received");
                    return;
                case Constants.CONTROLLER_FINISH_MEASURE_MSG:
                    Log.d(TAG, "CONTROLLER_FINISH_MEASURE_MSG received");

                    //aggiunto io
                    inviaFinito();
                    try {
                        sock.close();
                    } catch (IOException e) {
                        Log.e(TAG,e.getMessage());
                    }
                    //

                    //aggiunto io per DB
                    inviaDatiRaccoltiADatabase();
                    String risultato = ascoltaSentenza();
                    if(risultato.equals("rete neutrale")){

                    LocalBroadcastManager.getInstance(this).sendBroadcast(//da cambiare poi intent per visualizzare risultato
                            new Intent(Constants.INTENT_TEST_COMPLETED)
                    );}
                    if(risultato.equals("test fallito")){

                    LocalBroadcastManager.getInstance(this).sendBroadcast(
                            new Intent(Constants.INTENT_TEST_FAILED)
                    );}
                    if(risultato.equals("rete non neutrale")){//da cambiare poi intent per visualizzare risultato

                        LocalBroadcastManager.getInstance(this).sendBroadcast(
                                new Intent(Constants.INTENT_TEST_COMPLETED)
                        );}
                    return;
                case Constants.CONTROLLER_START_UB_MSG:
                    Log.d(TAG, "CONTROLLER_START_UB_MSG received");
                    port = getPort(receivedCommand);
                    if (port < 0)
                        return;
                    currentTest = btTest;
                    phase = Constants.TEST_UPLINK_PHASE;
                    action = Constants.INTENT_BT_UPLINK_PASSED;
                    break;
                case Constants.CONTROLLER_START_UC_MSG:
                    Log.d(TAG, "CONTROLLER_START_UC_MSG received");
                    port = getPort(receivedCommand);
                    if (port < 0)
                        return;
                    currentTest = ctTest;
                    phase = Constants.TEST_UPLINK_PHASE;
                    action = Constants.INTENT_CT_UPLINK_PASSED;
                    break;
                case Constants.CONTROLLER_START_UH_MSG:
                    Log.d(TAG, "CONTROLLER_START_UH_MSG received");
                    port = getPort(receivedCommand);
                    if (port < 0)
                        return;
                    currentTest = htTest;
                    phase = Constants.TEST_UPLINK_PHASE;
                    action = Constants.INTENT_HT_UPLINK_PASSED;
                    break;
                case Constants.CONTROLLER_START_DB_MSG:
                    Log.d(TAG, "CONTROLLER_START_DB_MSG received");
                    port = getPort(receivedCommand);
                    if (port < 0)
                        return;
                    currentTest = btTest;
                    phase = Constants.TEST_DOWNLINK_PHASE;
                    action = Constants.INTENT_BT_DOWNLINK_PASSED;
                    break;
                case Constants.CONTROLLER_START_DC_MSG:
                    Log.d(TAG, "CONTROLLER_START_DC_MSG received");
                    port = getPort(receivedCommand);
                    if (port < 0)
                        return;
                    currentTest = ctTest;
                    phase = Constants.TEST_DOWNLINK_PHASE;
                    action = Constants.INTENT_CT_DOWNLINK_PASSED;
                    break;
                case Constants.CONTROLLER_START_DH_MSG:
                    Log.d(TAG, "CONTROLLER_START_DH_MSG received");
                    port = getPort(receivedCommand);
                    if (port < 0)
                        return;
                    currentTest = htTest;
                    phase = Constants.TEST_DOWNLINK_PHASE;
                    action = Constants.INTENT_HT_DOWNLINK_PASSED;
                    break;
                case Constants.CONTROLLER_SEND_META_DATA_MSG:
                    Log.d(TAG, "CONTROLLER_SEND_META_DATA_MSG received");
                    metaData = new Hashtable<>();
                    try {
                        controller.sendCtrlMsg(Constants.CONTROLLER_OK_MSG, metaData);
                    } catch (ControllerException e) {
                        fallito = true;
                        LocalBroadcastManager.getInstance(this).sendBroadcast(
                                new Intent(Constants.INTENT_TEST_FAILED));
                        Log.e(TAG, "Unable to send metadata: " + e.getMessage());
                    }
                    continue;
                default:
                    Log.d(TAG, "Command not recognized");
                    return;
            }
            result = new Hashtable<>();
            try {
                tester = new Tester(port); // modificato io
                Log.d(TAG, "Connecting to the test channel");
                tester.connect(indirizzoIPServer); //modificato io
                Log.d(TAG, "Doing Test");
                tester.doTest(currentTest, phase, result);
                if (phase == Constants.TEST_UPLINK_PHASE) {
                    Log.d(TAG, "Sleeping due to UPLINK PHASE");
                    synchronized(this) {
                        try {
                            wait(10000);
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Interrupt Exception: "+ e.getMessage());
                        }
                    }
                }
                notifyAndSend(action);
                Log.d(TAG, "Closing test connection");
                tester.closeTestConnection();
                try {
                    Log.d(TAG, "Sending the results to server");
                    controller.sendCtrlMsg(Constants.CONTROLLER_OK_MSG, result);
                }catch (ControllerException e) {
                    fallito = true;
                    LocalBroadcastManager.getInstance(this).sendBroadcast(
                            new Intent(Constants.INTENT_TEST_FAILED));
                    e.printStackTrace();
                    Log.e(TAG, "Error while sending the results to the server, " + e.getMessage());
                    return;
                }
            } catch (TesterException e) {
                fallito = true;
                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        new Intent(Constants.INTENT_TEST_FAILED));
                Log.e(TAG, "Tester Exception: "+ e.getMessage());
                try {
                    switch (e.errorCode) {
                        case Constants.TESTER_CONNECT_TIMEOUT_ERROR:
                            controller.sendCtrlMsg(Constants.CONTROLLER_CLIENT_CONNECT_TIMEOUT_ERROR);
                            break;
                        case Constants.TESTER_CONNECT_GENERIC_ERROR:
                            controller.sendCtrlMsg(Constants.CONTROLLER_CLIENT_CONNECT_GENERIC_ERROR);
                            break;
                        case Constants.TESTER_TEST_ABORT_ERROR:
                            controller.sendCtrlMsg(Constants.CONTROLLER_CLIENT_TEST_ABORT_ERROR, result);
                            tester.closeTestConnection();
                            connector.closeConnection();
                            return;
                        case Constants.TESTER_TEST_GENERIC_ERROR:
                            controller.sendCtrlMsg(Constants.CONTROLLER_CLIENT_TEST_GENERIC_ERROR, result);
                            break;
                        case Constants.TESTER_INIT_CLIENT_ERROR:
                            controller.sendCtrlMsg(Constants.CONTROLLER_CLIENT_TEST_INIT_ERROR);
                            break;
                    }
                } catch (Exception ce) {
                    fallito = true;
                    LocalBroadcastManager.getInstance(this).sendBroadcast(
                            new Intent(Constants.INTENT_TEST_FAILED));
                    if (ce instanceof ControllerException)
                        Log.e(TAG, "Error while sending the test outcome to the server, " + ce.getMessage());
                    else
                        Log.e(TAG,"Error while closing connections, " + ce.getMessage());
                    return;
                }
            }
        }



    }
    @Override
    public void onDestroy(){
        stoppato = true;
        Log.d(TAG,"fermato TestService: chiamato onDestroy");
        super.onDestroy();
    }

    private void notifyAndSend(String action){
        Intent actionIntent = new Intent(action);
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        switch(action){
            case Constants.INTENT_BT_DOWNLINK_PASSED:
                //aggiunto io per DB
                throughput_bt = currentTest.downloadedDataRegister.getThroughputs();
                instant_bt = currentTest.downloadedDataRegister.getTimeInstants();
                mean_bt_th = currentTest.downloadedDataRegister.getThroughputMean();
                //
                actionIntent.putExtra(Constants.INTENT_THROUGHPUTS_ARRAY,
                    currentTest.downloadedDataRegister.getThroughputs())
                    .putExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY,
                            currentTest.downloadedDataRegister.getTimeInstants())
                    .putExtra(Constants.EXTRA_MEAN_TH, currentTest.downloadedDataRegister.getThroughputMean());
                lbm.sendBroadcast(actionIntent);
                break;
            case Constants.INTENT_CT_DOWNLINK_PASSED:
                //aggiunto io per DB
                throughput_ct = currentTest.downloadedDataRegister.getThroughputs();
                instant_ct = currentTest.downloadedDataRegister.getTimeInstants();
                mean_ct_th = currentTest.downloadedDataRegister.getThroughputMean();
                //
                actionIntent.putExtra(Constants.INTENT_THROUGHPUTS_ARRAY,
                        currentTest.downloadedDataRegister.getThroughputs())
                        .putExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY,
                                currentTest.downloadedDataRegister.getTimeInstants())
                        .putExtra(Constants.EXTRA_MEAN_TH, currentTest.downloadedDataRegister.getThroughputMean());
                lbm.sendBroadcast(actionIntent);
                break;
            case Constants.INTENT_HT_DOWNLINK_PASSED:
                //aggiunto io per DB
                throughput_ht = currentTest.downloadedDataRegister.getThroughputs();
                instant_ht = currentTest.downloadedDataRegister.getTimeInstants();
                mean_ht_th = currentTest.downloadedDataRegister.getThroughputMean();
                //
                actionIntent.putExtra(Constants.INTENT_THROUGHPUTS_ARRAY,
                        currentTest.downloadedDataRegister.getThroughputs())
                        .putExtra(Constants.INTENT_THROUGHPUT_INSTANTS_ARRAY,
                                currentTest.downloadedDataRegister.getTimeInstants())
                        .putExtra(Constants.EXTRA_MEAN_TH, currentTest.downloadedDataRegister.getThroughputMean());
                lbm.sendBroadcast(actionIntent);
                break;
            case Constants.INTENT_BT_UPLINK_PASSED:
                lbm.sendBroadcast(actionIntent);
                break;
            case Constants.INTENT_CT_UPLINK_PASSED:
                lbm.sendBroadcast(actionIntent);
                break;
            case Constants.INTENT_HT_UPLINK_PASSED:
                lbm.sendBroadcast(actionIntent);
                break;
        }
    }

    private int getPort(byte[] bytes) {
        if (bytes.length < 8) {
            Log.e(TAG, "No port available, ABORT");
            return -1;
        }
        return ByteBuffer.wrap(bytes, 4, 4).getInt();
    }

    /*
    Funzione che richiede al Load Balancer un server cui potersi connettere.
    Se il Load Balancer avvisa che i server sono tutti occupati restituisce false al chiamante,
    altrimenti ottiene indirizzo IP e porta del server e restituisce true al chiamante
     */
    private boolean inviaRichiestaServer(){
        try{
            int codice = Integer.parseInt(din.readUTF());
            if(codice==Constants.TUTTI_OCCUPATI){
                Log.d(TAG, "Tutti occupati");
                sock.close();
                return false;
            }
            if(codice==Constants.SERVER_TROVATO){
                indirizzoIPServer = din.readUTF();
                portaServer = din.readUTF();
                Log.d(TAG, indirizzoIPServer + " " + portaServer);
            }
            if(codice!=Constants.TUTTI_OCCUPATI&&codice!=Constants.SERVER_TROVATO)
                Log.d(TAG, "Codice ricevuto errato: " + codice);
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
            return false;
        }
        return true;
    }
    /*
    Funzione che avvisa il LoadBalancer che il server che era stato occupato per eseguire il test
    può essere reso libero
     */
    private void inviaFinito(){
        try{
            String codice = String.valueOf(Constants.FINE_OPERAZIONI);
            dout.writeUTF(codice);//codice che indica che ha finito
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
        }
    }
    /*
    Funzione che passa al database tutti i dati del test appena svolto
     */
    private void inviaDatiRaccoltiADatabase(){
        try{
            sock = new Socket(serverAddr,20002);//è IP del LoadBalancer, che è il solito del Database e porta del Database (al momento 20002)
            dout = new DataOutputStream(sock.getOutputStream());
            din = new DataInputStream(sock.getInputStream());
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = formatter.format(new java.util.Date(System.currentTimeMillis()));
            dout.writeUTF(timestamp);
            dout.writeDouble(mean_bt_th);
            dout.writeDouble(mean_ct_th);
            dout.writeDouble(mean_ht_th);
            dout.writeInt(throughput_bt.length);
            for(int i=0;i<throughput_bt.length;i++)
                dout.writeDouble(throughput_bt[i]);
            dout.writeInt(instant_bt.length);
            for(int i=0;i<instant_bt.length;i++)
                dout.writeDouble(instant_bt[i]);
            dout.writeInt(throughput_ct.length);
            for(int i=0;i<throughput_ct.length;i++)
                dout.writeDouble(throughput_ct[i]);
            dout.writeInt(instant_ct.length);
            for(int i=0;i<instant_ct.length;i++)
                dout.writeDouble(instant_ct[i]);
            dout.writeInt(throughput_ht.length);
            for(int i=0;i<throughput_ht.length;i++)
                dout.writeDouble(throughput_ht[i]);
            dout.writeInt(instant_ht.length);
            for(int i=0;i<instant_ht.length;i++)
                dout.writeDouble(instant_ht[i]);
            TelephonyManager manager = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            String provider = manager.getNetworkOperatorName();//coppia di funzioni per ottenere il nome del provider
            dout.writeUTF(provider);
            Log.d(TAG, "timestamp: " + timestamp);
            Log.d(TAG, "mean_bt_th: " + mean_bt_th);
            Log.d(TAG, "mean_ct_th: " + mean_ct_th);
            Log.d(TAG, "mean_ht_th: " + mean_ht_th);
            Log.d(TAG, "provider: " + provider);
        }
        catch(Exception e){
            Log.e(TAG, e.getMessage());
        }
    }
    private String ascoltaSentenza(){//Funzione che riceve il messaggio di esito del test
        String sentenza = null;
        try{
           sentenza = din.readUTF();
           sock.close();
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
        }
        return sentenza;
    }
}







