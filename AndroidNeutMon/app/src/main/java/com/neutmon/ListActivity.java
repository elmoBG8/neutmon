package com.neutmon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    ArrayList<String> fileList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Intent intent = getIntent();
        fileList = intent.getStringArrayListExtra(Constants.EXTRA_LOAD_FILENAME);
        if(fileList != null && fileList.size()>0) {
            String[] array = new String[fileList.size()];
            for (int i = 0; i < array.length; i++)
                array[i] = fileList.get(i);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.row, array);
            ListView listView = findViewById(R.id.list_view);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent returnIntent = getIntent();
                    returnIntent.putExtra(Constants.EXTRA_LOAD_FILENAME, ((TextView) view).getText());
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
            listView.setAdapter(adapter);
        }
    }
}
