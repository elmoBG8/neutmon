

public class Server{
    int portaServer;
    String indirizzoIPServer;
    int timeout;//tempo indicato in secondi dopo il quale il server deve tornare libero
    boolean occupato;//booleano che indica se il server sta effettuando operazioni con un client o meno
    //costruttore da chiamare in caso di indirizzo IP e porta del server indicati
    public Server(String ind, int p){
        portaServer = p;
        indirizzoIPServer = ind;
        occupato = false;
    }
    //costruttore da chiamare in caso di solo indirizzo IP del server indicato
    public Server(String ind){
        portaServer = 10000; //porta di default
        indirizzoIPServer = ind;
        occupato = false;
    }
    //funzione chiamata per memorizzare il timeout del server con il valore passato come parametro
    public void settaTimeout(int t){
        timeout = t;
    }
    //funzione chiamata per ottenere il riferimento al server e per svolgere operazioni con esso
    public Server usaServer(){
        occupato = true;
        return this;
    }
    //funzione chiamata al termine delle operazioni per rendere disponibile nuovamente il server
    public void liberaServer(){
        occupato = false;
    }
    //funzione chiamata per far capire al chiamante se può usare il server (se restituisce false) o no
    public boolean occupato(){
        return occupato;
    }
}
