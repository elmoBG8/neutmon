
import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;


public class LoadBalancer {//classe principale

    static int portaLoadBalancer;
    static int timeout;
    static List<Server> listServer;
    static int liberi;
    public static void main(String[] args) {//crea un oggetto LoadBalancer e su di esso chiama start()
        LoadBalancer lb = new LoadBalancer();
        lb.start();
    }
    /*
    effettua all'infinito i seguenti passaggi: crea un ServerSocket mettendosi in ascolto sul valore
    memorizzato in portaLoadBalancer; quando riceve richieste di connessione crea un oggetto Client;
    passa a tale oggetto il socket e il riferimento di se stesso per permettere l'accesso alla lista di 
    server all'oggetto Client e chiama il metodo start() dell'oggetto Client
    */
    public void start(){
        while(true){
            try(ServerSocket servs = new ServerSocket(portaLoadBalancer);){
                Socket sd = servs.accept();
                Client client = new Client(sd, this);
                client.start();
        }
        catch(Exception e){
            System.err.println(e.getMessage());
        }
        }
    }
    //leggiConfigFile memorizza ogni riga contenuta in config.txt come stringa nella lista di stringhe lines
    //restituisce al chiamante lines
    static List<String> leggiConfigFile(){
        List<String> lines = Collections.emptyList(); 
        try
        { 
            lines = Files.readAllLines(Paths.get("config.txt"), StandardCharsets.UTF_8); 
        } 
        catch (IOException e) 
        { 
            e.printStackTrace(); 
        } 
        return lines;
    }
    /*
    costruttore che per ogni riga del file rimuove i contenuti sintattici e recupera i valori di:
    porta di ascolto del Load Balancer, timeout dei server, indirizzo IP e porta (quest'ultima solo se 
    indicata) dei server. Chiama il costruttore Server(String ind, int p) o Server(String ind) a seconda
    della presenza o meno della porta del server e aggiunge l'oggetto così creato a listServer.
    Setta infine il timeout per ogni server della lista col valore precedentemente recuperato da file e
    la variabile liberi col numero totale di server memorizzati in lista
    */
    LoadBalancer(){
        List<String> l = leggiConfigFile();
        listServer = new ArrayList<Server>();
        for(String s : l){
            s = s.replace("{", "").replace("}", "").replace("[", "").replace("]", "").replace(",", "");
            if(s.contains("portaLB")){
                s = s.replace("portaLB", "").replace(":", "").replace(" ", "");
                portaLoadBalancer = Integer.parseInt(s);
            }
            if(s.contains("timeout")){
                s = s.replace("timeout", "").replace(":", "").replace(" ", "");
                timeout = Integer.parseInt(s);
            }
            if(s.contains("indirizzo")){
                if(s.contains("porta"))
                    listServer.add(new Server(s.substring(s.indexOf("\"")+1, s.lastIndexOf("\"")), 
                            Integer.parseInt(s.substring(s.indexOf("porta")).
                                    replace("porta", "").replace(":", "").replace(" ", ""))));
                else
                    listServer.add(new Server(s.substring(s.indexOf("\"")+1, s.lastIndexOf("\""))));
            }
        }
        for(Server s : listServer){
            s.settaTimeout(timeout);
        }
        liberi = listServer.size();
    }
    /*
    occupaServer mette il lock sulla variabile liberi e controlla se essa vale zero. 
    In tal caso non ci sono server disponibili e restituisce false al chiamante, 
    altrimenti decrementa liberi di uno e restituisce true al chiamante.
    Viene chiamata quando un client vuole effettuare operazioni con un server e solo se occupaServer 
    restituisce true il client effettua in seguito una ricerca per ottenere un server effettivamente libero
    */
    synchronized boolean occupaServer(){
        if(liberi==0){
            return false;
        }
        liberi --;
        return true;
    }
    /*
    liberaServer mette il lock sulle variabile liberi e listServer. Cerca il server passatole
    come parametro per renderlo nuovamente libero e in seguito incrementa di uno liberi.
    Viene chiamata quando un client ha terminato di effettuare le operazioni con il server che aveva
    precedentemente occupato
    */
    synchronized void liberaServer(Server s){
        boolean trovato = false;
        for(Server serv : listServer){
            if(serv.indirizzoIPServer.equals(s.indirizzoIPServer)&&serv.portaServer==s.portaServer){
                serv.liberaServer();
                System.out.println("liberato: " + serv.indirizzoIPServer);
                trovato = true;
                break;
            }
        }
        if(trovato == true)
            liberi ++;
        System.out.println(liberi + " liberi");
    }
    /*
    funzione che mette il lock sulla variabile listServer. Scorre quindi tutta la lista e restituisce
    il primo server libero che trova al client. 
    */
    synchronized Server prendiServerDaLista(){
        for(Server serv : listServer){
                if(!serv.occupato()){
            System.out.println("liberi " + liberi + " preso " + serv.indirizzoIPServer);
                    serv.usaServer();//ne occupo uno libero
                    return serv;
                }
        }
        return null;
    }
}
