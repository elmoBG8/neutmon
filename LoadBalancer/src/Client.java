
import java.io.*;
import java.net.*;

public class Client extends Thread {
    static final int SERVER_TROVATO = 1;//costante usata per la comunicazione tra load balancer e client 
    static final int FINE_OPERAZIONI = 2;//costante usata per la comunicazione tra load balancer e client 
    static final int TUTTI_OCCUPATI = 3;//costante usata per la comunicazione tra load balancer e client 
    LoadBalancer lb;
    Socket sd;
    DataInputStream din;
    DataOutputStream dout;
    Server s;
    boolean finitoOperazioni;
    /*
    costruttore che memorizza il LoadBalancer e il socket precedentemente creato dal LoadBalancer,
    crea gli stream di input e di output e inizializza a false il booleano finitoOperazioni
    */
    public Client(Socket sock, LoadBalancer loadB){
        lb = loadB;
        sd = sock;
        try{
            din = new DataInputStream(sd.getInputStream());
            dout = new DataOutputStream(sd.getOutputStream());
        }
        catch(Exception e){
            System.err.println(e.getMessage());
        }
        finitoOperazioni = false;
    }
    /*
    eseguita quando il chiamante effettua una start(). 
    Cerca un server che abbia la variabile occupato a false.
    Se lo trova attiva il timeout memorizzato nell'oggetto Server e attende che il client abbia terminato
    di eseguire le operazioni con il server.
    Se non lo trova termina.
    */
    public void run(){
        if(trovaServerLibero()){
            attivaTimeoutOperazioniServer();//sta in un thread a parte
            attesaFineClient();
        }
    }
    /*
    funzione che crea un thread che viene mandato in sleep per i secondi indicati dal timeout.
    Al suo risveglio effettua un controllo sulla variabile fineOperazioni. Se vale ancora false 
    rende il server che aveva occupato libero
    */
    void attivaTimeoutOperazioniServer(){
        Thread t = new Thread(){
            public void run(){
                try{
                    sleep(lb.timeout*1000);//così si libera dopo i secondi indicati da timeout
                }catch(Exception e){
                System.err.println(e.getMessage());
                }
                if(!controllaFineOperazioni()){//solo se non ho finito vuol dire che forse l'operazione 
                                               //sta durando troppo, quindi libero direttamente il server 
                                               //e chiudo socket
                    liberaServerPreso();
                    System.out.println("liberato server perche' le operazioni duravano troppo tempo");
                try {
                    din.close();
                    dout.close();
                    sd.close();
                } catch(Exception e){
                    System.err.println(e.getMessage());
                }
                }
            }
        };
        t.start();
    }
    /*
    funzione che ripetutamente si blocca sullo stream di input del socket finché non legge il codice
    di fine operazioni. Quando lo legge libera il server occupato e manda fineOperazioni a true
    */
    void attesaFineClient(){
        try {
            int codice = -1;
            do{
                codice = Integer.parseInt(din.readUTF());
                if(codice==FINE_OPERAZIONI)//codice di fine operazioni
                    liberaServerPreso();
            }while(codice!=FINE_OPERAZIONI);
            fineOperazioni();
            }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    /*
    funzione che controlla se il LoadBalancer possiede in lista un server libero. Se non ne ha scrive
    nello stream di output il codice per dire che i server sono tutti occupati e termina subito restituendo
    false. Altrimenti scorre la lista dei server del LoadBalancer controllando in ordine di memorizzazione
    se uno è libero. Quando ne trova uno manda nello stream di output il codice per dire che ne ha trovato
    uno e in seguito spedisce indirizzoIP e porta del server trovato. Marca il server trovato come occupato,
    si salva il riferimento del server trovato nella variabile s e restituisce true.
    */
    boolean trovaServerLibero(){
        if(!lb.occupaServer()){
            try {
                    String codice = String.valueOf(TUTTI_OCCUPATI);
                    dout.writeUTF(codice);//codice per dire che tutti i server sono occupati
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            return false;
        }
        s = lb.prendiServerDaLista();
        if(s==null)
            return false;
        try {
            String codice = String.valueOf(SERVER_TROVATO);
            dout.writeUTF(codice);//mando codice per dire che ho trovato server
            dout.writeUTF(s.indirizzoIPServer);
            dout.writeUTF(Integer.toString(s.portaServer));
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
                }
        return true;
    }
    /*
    nel caso in cui nella variabile s stia ancora il riferimento ad un server cerca nella lista di server
    contenuta nel LoadBalancer il server memorizzato in s e quando lo trova lo rende libero e gli toglie
    il riferimento
    */
    void liberaServerPreso(){
        if(s==null)
            return;
        lb.liberaServer(s);
        s = null;
        return; 
    }
    synchronized void fineOperazioni(){//chiamata da attesaFineClient() quando le operazioni con il server
                                       //sono terminate. Mette il lock su finitoOperazioni perché 
                                       //variabile condivisa col thread che si occupa del timeout
        finitoOperazioni = true;
    }
    synchronized boolean controllaFineOperazioni(){//chiamata da attivaTimeoutOperazioniServer() al termine
                                                   //della sleep. Mette il lock su finitoOperazioni perché
                                                   //variabile condivisa col thread principale
        return finitoOperazioni;
    }
}
