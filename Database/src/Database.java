
import java.net.*;
import java.sql.*;
import java.util.*;


public class Database {//classe principale

    static final String DBName = "jdbc:sqlite:DBNeutMon.db";
    static final int portaDatabase = 20002;
    static boolean inUso = false;//indica se il database è in uso da qualche thread 
                                 //per una scrittura o lettura
    public static void main(String[] args) {
        Database db = new Database();
        db.start();
    }
    /*
    effettua all'infinito i seguenti passaggi: crea un ServerSocket mettendosi in ascolto sul valore
    memorizzato in portaDatabase; quando riceve richieste di connessione crea un oggetto Client;
    passa a tale oggetto il socket e il riferimento di se stesso per permettere l'accesso al
    database all'oggetto Client e chiama il metodo start() dell'oggetto Client
    */
    public void start(){
        while(true){
            try(ServerSocket servs = new ServerSocket(portaDatabase);){
                Socket sd = servs.accept();
                Client client = new Client(sd, this);
                client.start();
        }
        catch(Exception e){
            System.err.println(e.getMessage());
        }
        }
    }
   Database(){//costruttore
       creaNuovaTabella();
   }
   /*
   funzione che crea, se non esiste, una tabella di nome test nel database indicato da DBName 
   con i campi il cui significato è spiegato di seguito
   */
    private static void creaNuovaTabella() {
        String sql = "CREATE TABLE IF NOT EXISTS test (\n"
                + "	timestamp text PRIMARY KEY,\n"//timestamp del momento dell'invio dei dati
                                                      //da parte del client Android
                + "	bittorrent real ,\n"//float indicante il throughput medio del traffico bittorrent
                + "	control real ,\n"//float indicante il throughput medio del traffico di controllo
                + "	http real ,\n"//float indicante il throughput medio del traffico HTTP
                + "	provider text,\n"//ISP usato dal client Android
                + "	lunghezzaarraybittorrent integer,\n"
                + "     arraybittorrent text ,\n"//elenco indicante il numero di bit inviati come traffico
                                                 //bittorrent
                + "	lunghezzaarrayistantibittorrent integer,\n"
                + "     arrayistantibittorrent text ,\n"//elenco indicante il tempo impiegato a scaricare
                                                        //la quantità di bit indicata da arraybittorrent
                + "	lunghezzaarraycontrol integer,\n"
                + "     arraycontrol text ,\n"//elenco indicante il numero di bit inviati come traffico
                                              //di controllo
                + "	lunghezzaarrayistanticontrol integer,\n"
                + "     arrayistanticontrol text ,\n"//elenco indicante il tempo impiegato a scaricare
                                                     //la quantità di bit indicata da arraycontrol
                + "	lunghezzaarrayhttp integer,\n"
                + "     arrayhttp text ,\n"//elenco indicante il numero di bit inviati come traffico
                                           //HTTP
                + "	lunghezzaarrayistantihttp integer,\n"
                + "     arrayistantihttp text \n"//elenco indicante il tempo impiegato a scaricare
                                                  //la quantità di bit indicata da arrayhttp
                
                + ");";
        
        try (Connection conn = DriverManager.getConnection(DBName);
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);//istruzione per creare la tabella con la query scritta nella stringa sql
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    /*
    funzione che inserisce una nuova riga nel database contenente il test appena svolto i cui dati
    sono passati per parametro
    */
    public boolean inserisciTest(Test t) {
        boolean riuscito = false;
        String sql = "INSERT INTO test(timestamp,bittorrent,control,http,provider,"
                + "lunghezzaarraybittorrent,arraybittorrent,lunghezzaarrayistantibittorrent,"
                + "arrayistantibittorrent,lunghezzaarraycontrol,arraycontrol,lunghezzaarrayistanticontrol,"
                + "arrayistanticontrol,lunghezzaarrayhttp,arrayhttp,lunghezzaarrayistantihttp,"
                + "arrayistantihttp) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = DriverManager.getConnection(DBName);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, t.timestamp);
            pstmt.setDouble(2, t.mediaBittorrent);
            pstmt.setDouble(3, t.mediaControl);
            pstmt.setDouble(4, t.mediaHttp);
            pstmt.setString(5, t.provider);
            pstmt.setInt(6, t.arrayBittorrent.length);
            pstmt.setString(7, cambiaDaArrayAStringa(t.arrayBittorrent));
            pstmt.setInt(8, t.arrayInstantsBittorrent.length);
            pstmt.setString(9, cambiaDaArrayAStringa(t.arrayInstantsBittorrent));
            pstmt.setInt(10, t.arrayControl.length);
            pstmt.setString(11, cambiaDaArrayAStringa(t.arrayControl));
            pstmt.setInt(12, t.arrayInstantsControl.length);
            pstmt.setString(13, cambiaDaArrayAStringa(t.arrayInstantsControl));
            pstmt.setInt(14, t.arrayHttp.length);
            pstmt.setString(15, cambiaDaArrayAStringa(t.arrayHttp));
            pstmt.setInt(16, t.arrayInstantsHttp.length);
            pstmt.setString(17, cambiaDaArrayAStringa(t.arrayInstantsHttp));
            pstmt.executeUpdate();
            riuscito = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally{
            return riuscito;
        }
    }
    /*
    funzione che recupera tutti i test salvati nella tabella test del database e li memorizza in una lista
    */
    public List<Test> vediTestPassati(){
        String sql = "SELECT timestamp, bittorrent, control, http, arraybittorrent, "
                + "arrayistantibittorrent, arraycontrol, arrayistanticontrol,"
                + "arrayhttp, arrayistantihttp, provider FROM test";
        List<Test> listaTest = new ArrayList<Test>();
        try (Connection conn = DriverManager.getConnection(DBName);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
                Test t = new Test(rs.getString("timestamp"),rs.getDouble("bittorrent"),
                        rs.getDouble("control"),rs.getDouble("http"),rs.getString("arraybittorrent"),
                        rs.getString("arrayistantibittorrent"),rs.getString("arraycontrol"),
                        rs.getString("arrayistanticontrol"),rs.getString("arrayhttp"),
                        rs.getString("arrayistantihttp"),rs.getString("provider"));
                listaTest.add(t);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listaTest;
    }
    static String cambiaDaArrayAStringa(double[] array){
        String stringaArray = Double.toString(array[0]);
        for(int i=1;i<array.length;i++)
            stringaArray +=","+Double.toString(array[i]);
        return stringaArray;
    }
    synchronized void prendiDB(){//funzione che gestisce la mutua esclusione del database tra i thread
        while(inUso==true)
            try {
                wait();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        inUso = true;
    }
    synchronized void liberaDB(){//funzione che gestisce la mutua esclusione del database tra i thread
        inUso = false;
        notifyAll();
    }
    
}
