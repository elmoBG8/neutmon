
import java.io.*;
import java.net.*;
import java.util.*;


public class Client extends Thread{
    Database db;
    Socket sd;
    DataInputStream din;
    DataOutputStream dout;
    public Client(Socket sock, Database datab){
        db = datab;
        sd = sock;
        try{
            din = new DataInputStream(sd.getInputStream());
            dout = new DataOutputStream(sd.getOutputStream());
        }
        catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    public void run(){
        //inizializzo le variabili che utilizzerò
        String timestamp = "", provider = "";
        double bittorrent = -1, control = -1, http = -1;
        double[] arrayBittorrent = null;
        double[] arrayIstantiBittorrent = null;
        double[] arrayControl = null;
        double[] arrayIstantiControl = null;
        double[] arrayHttp = null;
        double[] arrayIstantiHttp = null;
        boolean riuscito;
        try {//prendo valori da client android
        timestamp = din.readUTF();
        bittorrent = din.readDouble();
        control = din.readDouble();
        http = din.readDouble();
        arrayBittorrent = leggiArray(din.readInt());
        arrayIstantiBittorrent = leggiArray(din.readInt());
        arrayControl = leggiArray(din.readInt());
        arrayIstantiControl = leggiArray(din.readInt());
        arrayHttp = leggiArray(din.readInt());
        arrayIstantiHttp = leggiArray(din.readInt());
        provider = din.readUTF();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
        Test t = new Test(timestamp,bittorrent,control,http,arrayBittorrent,arrayIstantiBittorrent,
                arrayControl,arrayIstantiControl,arrayHttp,arrayIstantiHttp,provider);
        db.prendiDB();//metto il lock sul database, se libero, altrimenti vado in wait()
        riuscito = db.inserisciTest(t);
        db.liberaDB();//libero il lock sul database
        if(riuscito == true){
            db.prendiDB();
            List<Test> listaTest = db.vediTestPassati();
            db.liberaDB();
            System.out.println("misura presa correttamente alle " + java.time.LocalDateTime.now());
        }
        else
            System.out.println("misura abortita alle " + java.time.LocalDateTime.now());
        try {
            if(riuscito == true){
                dout.writeUTF("rete neutrale");//mando risultato che viene fuori dall'elaborazione dati
            }
            else
                dout.writeUTF("test fallito");
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    /*
    funzione che effettua tante letture dal socket quanto indicato dal parametro lunghezza
    salvando i risultati nell'array che viene passato al chiamante
    */
    private double[] leggiArray(int lunghezza){
        double[] array = new double[lunghezza];
        try{
        for(int i=0;i<array.length;i++)
            array[i] = din.readDouble();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
        return array;
    }
}
