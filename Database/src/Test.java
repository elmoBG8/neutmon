
import java.util.*;


public class Test {
    String timestamp;
    double mediaBittorrent;
    double mediaControl;
    double mediaHttp;
    double[] arrayBittorrent;
    double[] arrayInstantsBittorrent;
    double[] arrayControl;
    double[] arrayInstantsControl;
    double[] arrayHttp;
    double[] arrayInstantsHttp;
    String provider;
    public Test(String ts, double bt, double ct, double ht, double[] ab, double[] ib, double[] ac, 
            double[] ic, double[] ah, double[] ih, String pr){
        timestamp = ts;
        mediaBittorrent = bt;
        mediaControl = ct;
        mediaHttp = ht;
        arrayBittorrent = ab;
        arrayInstantsBittorrent = ib;
        arrayControl = ac;
        arrayInstantsControl = ic;
        arrayHttp = ah;
        arrayInstantsHttp = ih;
        provider = pr;
    }
    public Test(String ts, double bt, double ct, double ht, String ab, String ib, String ac, 
            String ic, String ah, String ih, String pr){
        timestamp = ts;
        mediaBittorrent = bt;
        mediaControl = ct;
        mediaHttp = ht;
        String [] pezzi;
        pezzi = ab.split(",");
        //procedimento per convertire un array di stringhe in array di double
        arrayBittorrent = Arrays.asList(pezzi).stream().mapToDouble(Double::parseDouble).toArray();
        pezzi = ib.split(",");
        arrayInstantsBittorrent = Arrays.asList(pezzi).stream().mapToDouble(Double::parseDouble).toArray();
        pezzi = ac.split(",");
        arrayControl = Arrays.asList(pezzi).stream().mapToDouble(Double::parseDouble).toArray();
        pezzi = ic.split(",");
        arrayInstantsControl = Arrays.asList(pezzi).stream().mapToDouble(Double::parseDouble).toArray();
        pezzi = ah.split(",");
        arrayHttp = Arrays.asList(pezzi).stream().mapToDouble(Double::parseDouble).toArray();
        pezzi = ih.split(",");
        arrayInstantsHttp = Arrays.asList(pezzi).stream().mapToDouble(Double::parseDouble).toArray();
        provider = pr;
    }
}
